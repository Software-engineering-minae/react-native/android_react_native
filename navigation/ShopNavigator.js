import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContent, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { Avatar, Title, TouchableRipple, Caption, Switch } from 'react-native-paper';
import { Ionicons, FontAwesome5, MaterialIcons } from '@expo/vector-icons';
import { View, SafeAreaView, Button, Text, TouchableOpacity  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import CustomHeaderButton from '../components/UI/HeaderButton';


import * as authActions from '../store/actions/auth';

import ProductsOverviewScreen from '../screens/shop/ProductsOverviewScreen';
import AuthScreen from '../screens/user/AuthScreen';

import Colors from '../constants/Colors';
import ProductDetailScreen from '../screens/shop/ProductDetailScreen';
import UserProductsScreen from '../screens/user/UserProductsScreen';
import HomeScreen from '../screens/shop/HomeScreen';
import SearchScreen from '../screens/shop/SearchScreen';
import CartScreen from '../screens/shop/CartScreen';
import CartInfoScreen  from '../screens/shop/CartInfoScreen';
import EditProductScreen from '../screens/user/EditProductScreen';
import ProfileScreen from '../screens/user/ProfileScreen';
import EditInfoScreen from '../screens/user/EditInfoScreen';
import UserAddress from '../screens/user/UserAddress';
import OrdersScreen from '../screens/shop/OrdersScreen';
import SalesRecordScreen from '../screens/user/SalesRecordScreen';
import { TouchableHighlight } from 'react-native-gesture-handler';
import AddCreditScreen from '../screens/user/AddCreditScreen';

const defaultNavOptions = {
  headerStyle: {
    backgroundColor: Colors.primary
  },
  headerTintColor:  'white',
  headerTitleAlign: 'center' ,
}

const defaultNavOptionsForDrawer = ({route, navigation}) => ({
    headerStyle: {
        backgroundColor: Colors.primary
      },
      headerTintColor:  'white',
      headerTitleAlign: 'center' ,
      headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
            <Item 
                title="Menu"
                iconName='md-menu'
                onPress ={  () => {navigation.toggleDrawer()} }
            />
        </HeaderButtons>
    ),
    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
            <Item 
                title="Cart"
                iconName='md-cart'
                onPress ={ () => {
                navigation.navigate('Cart')
                } }
            />
        </HeaderButtons>
    ),
    
    
})

const Drawer_Content = (props) => {
    const dispatch = useDispatch();
    const username = useSelector(state => state.auth.userInfo.username)
    const credit = useSelector(state => state.auth.userInfo.credit)
    const logout = () => {
        dispatch(authActions.logout())
    }
    return(
    <View style={{marginTop: 20}} >
        <Avatar.Image style={{marginHorizontal: 10, marginVertical: 5}} source={require('../assets/profile.jpg')} size={50} />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}} >
            <Title style={{margin: 10}} > {username} </Title>
            <Text style={{ paddingHorizontal: 5, left: 5, marginTop: 8, marginLeft: 55, height:20, backgroundColor: 'white', borderRadius: 10}} > {credit}$ </Text>
            <TouchableOpacity style={{flexDirection: 'row',  marginHorizontal: 20, marginVertical: 4}} onPress={()=>{props.navigation.navigate('Products', {
                screen: 'AddCredit'
            })}} >
                <Ionicons name="md-add" size={25} style={{top: 2}} />
            </TouchableOpacity>
        </View>
        <DrawerItemList {...props} />
        <TouchableOpacity style={{flexDirection: 'row', marginHorizontal: 20, marginTop: 15}} onPress={()=>{logout()}} >
            <Ionicons name="md-exit" color="#ff1123" size={26} />
            <Text style={{fontSize:16, marginHorizontal: 30, color:'#ff1123'}} > Logout </Text>
        </TouchableOpacity>
    </View>
)
    }
const navOptionsForProfile = ({route, navigation}) => ({
    headerStyle: {
        backgroundColor: Colors.primary
      },
      headerTintColor:  'white',
      headerTitleAlign: 'center' ,
      headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
            <Item 
                title="Menu"
                iconName='md-menu'
                onPress ={  () => {navigation.toggleDrawer()} }
            />
        </HeaderButtons>
    ),
    // headerRight: () => (
    //     <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
    //         <Item 
    //             title="Cart"
    //             iconName='md-cart'
    //             onPress ={ () => {
    //             navigation.navigate('Cart')
    //             } }
    //         />
    //     </HeaderButtons>
    // )
    
})


const ProductsStack = createStackNavigator();
const SwitchStack = createStackNavigator();
const UserStack = createStackNavigator();
const SearchStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const CartStack = createStackNavigator();
const EditInfoStack = createStackNavigator();
const OrderStack = createStackNavigator();
const SaleStack = createStackNavigator();
const CreditStack = createStackNavigator();

const ProfileDrawer = createDrawerNavigator();
const ShopNavigator = createDrawerNavigator();

const MainNavigator = () => {
  const token = useSelector(state => state.auth.token)
  const isProducer = useSelector(state => state.auth.userInfo.isProducer)
  const dispatch = useDispatch();
  const logout = () => {
      dispatch(authActions.logout())
  }
  return(
    <NavigationContainer>
      {
        !token ? (
            <SwitchStack.Navigator screenOptions={defaultNavOptions} >
                <SwitchStack.Screen name="Auth" component={AuthScreen} options={{title:'Authenticate'}} />
            </SwitchStack.Navigator>
        )
        : (
            <ShopNavigator.Navigator drawerStyle={{backgroundColor: '#C9D6FF'}} drawerContent={ props => <Drawer_Content {...props} /> } >
                <ShopNavigator.Screen name="Products" component={ProductsNavigator} options={{
                    drawerIcon: drawerConfig => <Ionicons name="md-list" size={23} color={drawerConfig.color} />
                }} />
                <ShopNavigator.Screen name="Search" component={SearchNavigator} options={{
                    drawerIcon: drawerConfig => <FontAwesome5 name="search" size={23} color={drawerConfig.color} />
                }} />
                { isProducer ? 
                    <ShopNavigator.Screen name="Profile" component={UserNavigator} options={{
                    drawerIcon: drawerConfig => <FontAwesome5 name="user-alt" size={23} color={drawerConfig.color} />
                }} />
                :
                    null
                }
                <ShopNavigator.Screen name="Edit info" component={EditInfoNavigator} options={{
                    drawerIcon: drawerConfig => <FontAwesome5 name="user-edit" size={23} color={drawerConfig.color}/>,
                }} />
                <ShopNavigator.Screen name="Cart" component={CartNavigator} options={{
                    drawerIcon: drawerConfig => <Ionicons name="md-cart" size={23} color={drawerConfig.color} />
                }} />
                <ShopNavigator.Screen name="Orders History" component={OrdersNavigator} options={{
                    drawerIcon: drawerConfig => <MaterialIcons name="reorder" size={23} color={drawerConfig.color} />
                }}  />
                <ShopNavigator.Screen name="Sales Record" component={SalesNavigator} options={{
                    drawerIcon: drawerConfig => <MaterialIcons name="shopping-basket" size={23} color={drawerConfig.color} />
                }}  />
            </ShopNavigator.Navigator>
        )
      }
    </NavigationContainer>
  )
}

const ProductsNavigator = () => {

    const username = useSelector(state => state.auth.userInfo.username)

    return (
            <ProductsStack.Navigator screenOptions={defaultNavOptionsForDrawer} >
                <ProductsStack.Screen name= "Home" component={HomeScreen} options={({navigation}) => ({
                    title: `${username ? `welcome ${username}` : ''}`
                })} />
                <ProductsStack.Screen name= "ProductsOverview" component={ProductsOverviewScreen} options={({route, navigation}) => ({
                    title: route.params?.productsType,
                    
                    })} />
                <ProductsStack.Screen 
                    name= "ProductsDetail" 
                    component={ProductDetailScreen} 
                    options= { ({route, navigation}) => ({
                        title: route.params?.productTitle, 

                }) } />
                <ProductsStack.Screen name="Search" component={SearchScreen} />
                <ProductsStack.Screen name= "Cart" component={CartScreen} />
                <ProductsStack.Screen name= "CartInfo" component={CartInfoScreen} />
                <ProductsStack.Screen name= "AddCredit" component={AddCreditScreen} />
            </ProductsStack.Navigator>
    )
}


const SearchNavigator = () => {
    return (
            <SearchStack.Navigator screenOptions={defaultNavOptionsForDrawer} >
                <SearchStack.Screen name="Search" component={SearchScreen} />
                <SearchStack.Screen name="ProductsDetail" component={ProductDetailScreen} />
                <SearchStack.Screen name= "AddCredit" component={AddCreditScreen} />
            </SearchStack.Navigator>
    )
}

const EditInfoNavigator = () => {
    return (
        <EditInfoStack.Navigator screenOptions={navOptionsForProfile} >
            <EditInfoStack.Screen name="EditInfo" component={EditInfoScreen} options={({navigation, route}) => ({
                headerRight: () => (
                    <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
                            <Item 
                            title="Add"
                            iconName='md-locate'
                            onPress ={ () => {
                                navigation.navigate('UserAddress')
                            } }
                            />
                    </HeaderButtons>
                )
            })} />
            <EditInfoStack.Screen name="UserAddress" component={UserAddress} />
            <EditInfoStack.Screen name= "AddCredit" component={AddCreditScreen} />
        </EditInfoStack.Navigator>
    )
}

const UserNavigator = () => {
    return (
            <UserStack.Navigator screenOptions={navOptionsForProfile} >
                <UserStack.Screen  name="Profile" component={ProfileScreen} />
                <UserStack.Screen name= "AddCredit" component={AddCreditScreen} />
                {/* <UserStack.Screen name="UserProduct" component={UserProductsScreen} options={({route, navigation}) => ({
                    headerRight: () => (
                        <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
                            <Item 
                            title="Add"
                            iconName='md-add'
                            onPress ={ () => {
                                navigation.navigate('EditProduct', {
                                    edit: false
                                })
                            } }
                            />
                        </HeaderButtons>
                    )
                })} 
                /> */}
                <UserStack.Screen name="EditProduct" component={EditProductScreen} options={({route, navigation}) => ({
                    headerRight: () => (
                        null
                    )
                })} />
            </UserStack.Navigator>
    )
}


const CartNavigator = () => {
    return(
        <CartStack.Navigator screenOptions={navOptionsForProfile} >
            <CartStack.Screen name="Cart" component={CartScreen} />
            <CartStack.Screen  name="Orders" component={OrdersScreen} />
            <CartStack.Screen name= "AddCredit" component={AddCreditScreen} />
        </CartStack.Navigator>
    )
}

const OrdersNavigator = () => {
    return (
        <OrderStack.Navigator screenOptions={defaultNavOptionsForDrawer} >
            <OrderStack.Screen name="Orders" component={OrdersScreen} />
            <OrderStack.Screen name= "AddCredit" component={AddCreditScreen} />
        </OrderStack.Navigator>
    )
}

const SalesNavigator = () => {
    return (
        <SaleStack.Navigator screenOptions={defaultNavOptionsForDrawer} >
            <SaleStack.Screen name="Sales Rocord" component={SalesRecordScreen} />
            <SaleStack.Screen name= "AddCredit" component={AddCreditScreen} />
        </SaleStack.Navigator>
    )
}

export default MainNavigator;