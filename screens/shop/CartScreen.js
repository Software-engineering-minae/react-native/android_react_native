import React, { useState, useEffect } from 'react';
import { View, Text, Image, FlatList, StyleSheet, Button, ActivityIndicator, TouchableOpacity } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import * as cartActions from '../../store/actions/cart';
import * as orderActions from '../../store/actions/orders';
import * as authActions from '../../store/actions/auth';
import Address from '../../components/UI/Address';

import Colors from '../../constants/Colors';
import CartItem from '../../components/Shop/CartItem';
import { MaterialIcons } from '@expo/vector-icons';
import Dialog from 'react-native-dialog';

const CartScreen = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [isVisible, setIsVisible] = useState(false);
    const [selected, setSelected] = useState(false);
    const userAddress = useSelector(state => state.auth.Address)
    const selectedAddress = useSelector(state => state.cart.selectedAddress)
    const currentCredit = useSelector(state => state.auth.userInfo.credit)
    const dispatch = useDispatch();
    const cartTotalAmount = useSelector(state => state.cart.total)
    const cartItems = useSelector(state => {
        const transformedCartItems = []
        for( const key in state.cart.items ) {
            transformedCartItems.push({
                productId: key,
                productImage: state.cart.items[key].productImage,
                productTitle: state.cart.items[key].productTitle,
                productPrice: state.cart.items[key].productPrice,
                productDescription:  state.cart.items[key].productDescription,
                quantity: state.cart.items[key].quantity,
                sum: state.cart.items[key].sum
            })
        }
        return transformedCartItems.sort((a,b) => a.productId > b.productId ? 1 : -1 );
    })
    

    useEffect(() => {
        const retrieveCart = async () => {
            setIsLoading(true)
            await dispatch(cartActions.retieveCart())
            setIsLoading(false)
        }
        retrieveCart()
    },[dispatch])

    const onAddHandler = async (prodId) => {
        setIsLoading(true)
        await dispatch(cartActions.addByOne(prodId))
        setIsLoading(false)
    }
    const onRemoveHandler = async (prodId) => {
        setIsLoading(true)
        await dispatch(cartActions.removeByOne(prodId))
        setIsLoading(false)
    }
    const onDeleteHandler = async (prodId) => {
        setIsLoading(true)
        await dispatch(cartActions.removeFromCart(prodId))
        setIsLoading(false)
    }
    const onOrderHandler = async (orderTotalPrice, addressId) => {
        let screen;
        setIsLoading(true)
        await dispatch(orderActions.addOrder(orderTotalPrice, addressId))
        if (orderTotalPrice > currentCredit) {
            screen = 'AddCredit'
        }
        else {
            screen = 'Home'
        }
        await dispatch(authActions.retrieveProfile())
        props.navigation.navigate('Products', {
            screen: screen
        })
    }

    const getFooter = () => {
        if (!isLoading) {
            return (
                <View style={{alignItems: 'flex-end', height: 200, width: '100%'}} >
                    <TouchableOpacity style={styles.touchable} onPress={() => {
                        setSelected(false)
                        setIsVisible(true)

                        } } >
                        <Text style={{paddingTop: 2, paddingRight: 1, marginRight:2}} > Add an address </Text>
                        <MaterialIcons name="add-location" size={25}  />
                    </TouchableOpacity>
                    <Dialog.Container visible={isVisible}>
                        <Dialog.Title> Add Address </Dialog.Title>
                        <FlatList 
                            data={userAddress}
                            keyExtractor={item => item.id.toString()}
                            renderItem={ itemData => <TouchableOpacity style={{flexDirection: 'row', elevation: 3, marginVertical: 10 }} onPress={ () => {
                                dispatch(cartActions.selectAddress(itemData.item))
                                console.log("this is selected Address when click on!:  ", selectedAddress)
                                setIsVisible(false)
                            } } >
                                <MaterialIcons name="radio-button-unchecked" size={25} />
                                <Text style={{paddingHorizontal: 5, paddingTop: 2}} > {itemData.item.province}/{itemData.item.city}/{itemData.item.address} </Text>
                            </TouchableOpacity> }
                        />
                    </Dialog.Container>


               </View>
            )
        }
        else {
            return (
                <View>

                </View>
            )
        }
    }


    return(
        <View style={styles.screen} >
            <View style={styles.summary} >
                <Text style={styles.summaryText} > Total: <Text style={styles.amount} > ${cartTotalAmount} </Text>
                </Text>
                <Button title="Order Now" onPress={ () => {
                    onOrderHandler(cartTotalAmount, selectedAddress.id)
                } } disabled={cartItems.length === 0} />
            </View>
            {!isLoading ? 
            <FlatList
                data={cartItems}
                keyExtractor={item => item.productId} 
                renderItem={ itemData => 
                <CartItem
                    quantity={itemData.item.quantity}
                    image={itemData.item.productImage}
                    title={itemData.item.productTitle}
                    amount={itemData.item.sum}
                    action
                    onDelete={ () => {
                        onDeleteHandler(itemData.item.productId)
                    } }
                    onAdd={ () => {
                        onAddHandler(itemData.item.productId)
                    } }
                    onRemove={ () => {
                        onRemoveHandler(itemData.item.productId)
                    } }
                />
                }
                ListFooterComponent={getFooter()}
            />

            : 
            <ActivityIndicator size="large" color={Colors.primary} style={styles.spinner} />    
            }
        </View>
    )
}


const styles = StyleSheet.create({
    screen: {
        margin: 13,
    },
    summary: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
        padding: 10,
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white'
    },
    summaryText: {
        fontWeight: 'bold',
        fontSize: 18
    },
    amount: {
        color: Colors.accent
    },
    spinner: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    touchable: {
        marginRight: 5,
        paddingVertical: 5, 
        paddingHorizontal: 2,  
        marginTop: 5,  
        flexDirection: 'row-reverse', 
        elevation: 2, 
        borderRadius: 10
    }
})

export default CartScreen;