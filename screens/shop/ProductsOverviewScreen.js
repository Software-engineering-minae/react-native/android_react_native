import React, { useEffect, useState} from 'react';
import { FlatList, ActivityIndicator, View, StyleSheet, Button, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import GestureRecognizer from 'react-native-swipe-gestures';

import ProductItem from '../../components/Shop/ProductItem';
import * as cartActions from '../../store/actions/cart';
import * as productActions from '../../store/actions/products';
import Colors from '../../constants/Colors';


const ProductsOverviewScreen = props => {
  const [isLoading, setIsLoading] = useState(false)
  let pageNo = useSelector(state => state.products.pageNoAvailableProducts)
  const productType = props.route.params?.productsType
  const products =  useSelector(state => state.products.availableProducts);
  const total = useSelector(state => state.products.totalAvailableProducts);
  const totalPage = total % 8 == 0 ? Math.floor(total / 8) : Math.floor(total / 8) + 1
  

  const dispatch = useDispatch();
  useEffect(() => {
    const loadProducts = async () => {
      setIsLoading(true)
      await dispatch(productActions.fetchTotalAvailableProducts(productType))
      await dispatch(productActions.fetchProductByCategoryType(productType, pageNo))
      setIsLoading(false)
    }
    loadProducts()
   },[dispatch])

   const onAddToCartHandler = async (prod) => {
    setIsLoading(true)
    await dispatch(cartActions.addToCart(prod))
    setIsLoading(false)
   }

   const selectItemHandler = (id, title) => {
      props.navigation.navigate('ProductsDetail', {
      productId: id,
      productTitle: title,
      search: false
    })
   }

   const onGoToNextScreen = async (cat,pageNo,status) => {
     setIsLoading(true)
     await dispatch(productActions.changePage(pageNo,status))
     await dispatch(productActions.fetchProductByCategoryType(cat,pageNo))
     setIsLoading(false)
    }

   if (isLoading) {
     return (
     <View style={styles.spinner}>
       <ActivityIndicator size="large" color={Colors.primary} />
     </View>
     )
   }
  return (
    <GestureRecognizer
      config={{
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80
      }}
      style={{flex:1}}
      onSwipeLeft={ () => {
        console.log("this is pageNo when swipe left : ", pageNo)
        if (totalPage > 1  && pageNo != totalPage) {
          pageNo += 1;
          onGoToNextScreen(productType,pageNo,'+')
        }
      } }
      onSwipeRight={ () => {
        console.log("this is pageNo when swipe right : ", pageNo)
        if (pageNo != 1) {
          pageNo -= 1;
          onGoToNextScreen(productType,pageNo,'-')
        }
      } }
    >
      <FlatList
        data={products}
        keyExtractor={item => item.id.toString()}
        renderItem={itemData => <ProductItem
            image={itemData.item.imageUrl}
            title={itemData.item.title}
            price={itemData.item.price}
            onSelect={ () => {
              selectItemHandler(itemData.item.id, itemData.item.title)
            } }      
          >
            <Button color={Colors.primary} title="Details" onPress={() => {
              selectItemHandler(itemData.item.id, itemData.item.title)
            }} />
            <Button color={Colors.primary} title="To Cart" onPress={() => {
              onAddToCartHandler(itemData.item)
            }} />
          </ProductItem> }
      />
    </GestureRecognizer>
  );
};


const styles = StyleSheet.create({
  spinner: {
    flex:1, 
    alignItems:'center', 
    justifyContent:'center'
  }
})

export default ProductsOverviewScreen;
