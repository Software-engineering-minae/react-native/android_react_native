import React, { useReducer, useState, useEffect } from 'react';
import {ScrollView, View, Text, Image, Button, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import StarRating from 'react-native-star-rating';
import Dialog from 'react-native-dialog';
import { Ionicons } from '@expo/vector-icons';

import { useSelector, useDispatch } from 'react-redux';
import * as cartActions from '../../store/actions/cart';

import Colors from '../../constants/Colors';
import ProductDetail from '../../components/Shop/ProductDetail';

import * as productActions from '../../store/actions/products';
import Comment from '../../models/comment';


const ProductDetailScreen = props => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [isVisible, setIsVisible] = useState(false);
    const [text, setText] = useState('')
    const productId = props.route.params?.productId
    const search = props.route.params?.search
    let selectedProduct;
    if (search) {
        selectedProduct = useSelector(state => state.products.searchedProducts.find( prod => prod.id === productId))
    }
    else {
        selectedProduct = useSelector(state => state.products.availableProducts.find( prod => prod.id === productId))
    }

    const allComments = useSelector(state => state.products.productComment)
    const rate = useSelector(state => state.products.productRate)
    const [rateIsLoading, setRateIsLoading] = useState(false)

    useEffect(() => {
        const loadComments = async () => {
          setIsLoading(true)
          setRateIsLoading(true)
          await dispatch(productActions.fetchComment(productId))
          await dispatch(productActions.getProductRate(productId))
          setRateIsLoading(false)
          setIsLoading(false)
        }
        loadComments()
       },[dispatch])

    const getHeader = () => {
        //for image uri: selectedProduct.imageUrl
        if (!isLoading) {
            return (
            <View style={{flex: 1, width: '100%', alignItems: 'center'}} > 
                <Image style={styles.image} source={{uri: selectedProduct.imageUrl }} />
                <View style={styles.action} > 
                    <Button color={Colors.primary} title='Add To Cart' onPress={ () => {
                        onAddToCartHandler(selectedProduct)
                    } } />
                    
                </View>
                <Text style={styles.price}>${selectedProduct.price}</Text>
                <Text style={styles.description} >{selectedProduct.description}</Text>
                <View style={styles.star} >
                    <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={rate}
                        selectedStar = { (rating) => {
                            rateChangeHandler(productId,rating)
                        }  } 
                        fullStarColor="gold"
                    />
                </View>
                <FlatList
                    data={selectedProduct.hashtags.split(',')}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => item.toString() + index.toString()}
                    renderItem={ itemData => <TouchableOpacity style={styles.hashtags} onPress={ () => onHashtagsTouchHandler(itemData.item) } >
                        <Text style={{fontSize: 16}} > {itemData.item} </Text>
                    </TouchableOpacity> }
                /> 

            </View>
            )
        }
        else {
            return (
                <ActivityIndicator size='large' color={Colors.primary} style={styles.spinner} />
            )
        }
    }

    const getFooter = () => {
        if (!isLoading) {
            return (
            <View style={{alignItems: 'flex-end', height: 200, width: '100%'}} >
                <TouchableOpacity style={styles.touchable} onPress={() => {
                    setIsVisible(true)
                }} >
                    <Ionicons 
                        size={25}
                        name="md-chatbubbles"
                    />
                </TouchableOpacity>
                <Dialog.Container visible={isVisible} >
                    <Dialog.Title> New Comment </Dialog.Title>
                    <Dialog.Description> Write your opinion about this below </Dialog.Description>
                    <Dialog.Input placeholder="Enter your text here"  onChangeText={ (text) => setText(text) } value={text} />
                    <Dialog.Button label="Add" onPress={() => {
                        setIsVisible(false)
                        onAddCommentHandler(productId, text)
                    }} />
                    <Dialog.Button label="Cancel" onPress={() => {
                        setIsVisible(false)
                    }} />
                </Dialog.Container>
            </View>
            )
        }
        else {
            return (
                <View>
                
                </View>
            )
        }
    }
    
    const likeChangeHandler =  async (comment, allComments) => {
        setIsLoading(true)
        await dispatch(productActions.checkLikeOrDislike(comment))
        await dispatch(productActions.like(comment, allComments))
        setIsLoading(false)
    }

    const dislikeChangeHandler = async (comment, allComments) => {
        setIsLoading(true)
        await dispatch(productActions.checkLikeOrDislike(comment))
        await dispatch(productActions.dislike(comment, allComments))
        setIsLoading(false)
    }

    const rateChangeHandler = async (prodId, rate) => {
        setRateIsLoading(true)
        await dispatch(productActions.addRate(prodId, rate))
        setRateIsLoading(false)
    }

    const onAddToCartHandler = async (prod) => {
        setIsLoading(true)
        await dispatch(cartActions.addToCart(prod))
        setIsLoading(false)
    }

    const onAddCommentHandler = async (prodId, text) => {
        setIsLoading(true)
        await dispatch(productActions.addComment(prodId, text))
        await dispatch(productActions.fetchComment(prodId))
        await dispatch(productActions.getProductRate(prodId))
        setIsLoading(false)
    }

    const onHashtagsTouchHandler = (hashtag) => {
        props.navigation.navigate('Search', {
            hashtag: hashtag
        })
    }


    if (!isLoading) {
        return (
            <FlatList 
                extraData={text}
                data={allComments}
                keyExtractor={ item => item.id.toString() }
                renderItem={itemData => <ProductDetail
                    like={itemData.item.like}
                    dislike={itemData.item.dislike}
                    likeHandler={ () => likeChangeHandler(itemData.item, allComments)}
                    dislikeHandler={ () => dislikeChangeHandler(itemData.item, allComments)}
                    commenter={itemData.item.commenter}
                    comment={itemData.item.comment}
                />
                }
                ListHeaderComponent={ getHeader() }
                ListFooterComponent={ getFooter() }
            />
        )
    }
    else {
        return (
            <ActivityIndicator size='large' color={Colors.primary} style={styles.spinner} />
        )

    }
        
}

ProductDetailScreen.navigationOptions = navData => {
    return {
        headerTitle: navData.navigation.getParam('productTitle')
    }
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 300
    },
    action: {
        marginVertical: 10,
        alignItems:'center'
    },
    price: {
        fontSize: 20,
        color: '#888',
        textAlign: 'center',
        marginVertical: 20,
    },
    description: {
        fontSize: 14,
        textAlign: 'center',
        fontWeight: 'bold',
        marginHorizontal: 15
    },
    star: {
        marginVertical: 10,
        alignItems: 'center'
    },
    spinner: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    touchable: {
        marginRight: 10,
        padding: 20
    },
    hashtags: {
        marginHorizontal: 4, 
        marginVertical: 3,
        backgroundColor: '#E6E7E8'
    }
})

export default ProductDetailScreen;