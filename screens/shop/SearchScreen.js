import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator, Button } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import GestureRecognizer from 'react-native-swipe-gestures';

import * as productActions from '../../store/actions/products';
import * as cartActions from '../../store/actions/cart';

import Input from '../../components/UI/Input';
import ProductItem from '../../components/Shop/ProductItem';
import { Ionicons } from '@expo/vector-icons';
import Colors from '../../constants/Colors';
import CustomHeaderButton from '../../components/UI/HeaderButton';

const SearchScreen = props => {
    const hashtag = props.route.params?.hashtag;
    let pageNo = useSelector(state => state.products.pageNoSearchedProducts)
    const [text, setText] = useState(hashtag ? hashtag : '');
    const [isLoading, setIsLoading] = useState(false)

    const total = useSelector(state => state.products.totalSearchedProducts)
    const totalPage = total % 8 == 0 ? Math.floor(total / 8) : Math.floor(total / 8) + 1

    const dispatch = useDispatch();
    const searchedProducts = useSelector(state => state.products.searchedProducts)
    const onSearchClick = async (search,page) => {
        setIsLoading(true)
        await dispatch(productActions.setSearchPage())
        await dispatch(productActions.fetchTotalSearchedProducts(search))
        await dispatch(productActions.searchProduct(search,page))
        setIsLoading(false)
    }
    
    useEffect(()=>{
        const onFirstLoad = async (search, page) => {
            setIsLoading(true)
            await dispatch(productActions.fetchTotalSearchedProducts(search))
            await dispatch(productActions.searchProduct(search, page))
            setIsLoading(false)
        }
        onFirstLoad(hashtag ? hashtag : '', pageNo)
    }, [dispatch])

    const getFooter = () => {
        return (
            <View style={{marginVertical: 5, width:'100%', height: 10}} >
            </View>
        )
    }
    const selectItemHandler = (id, title) => {
        props.navigation.navigate('Products', {
            screen : 'ProductsDetail',
            params: {
                productId: id,
                productTitle: title,
                search: true
            }
      })
    }

    const onAddToCartHandler = async (prod) => {
        setIsLoading(true)
        await dispatch(cartActions.addToCart(prod))
        setIsLoading(false)
       }
    
    const goToNextScreen = async (search,pageNo,status) => {
        setIsLoading(true)
        await dispatch(productActions.changeSearchPage(pageNo,status))
        await dispatch(productActions.searchProduct(search,pageNo))
        setIsLoading(false)
    }

    if(isLoading) {
        return (
            <View style={{flex:1, justifyContent:'center', alignItems: 'center'}} >
                <ActivityIndicator size="large" color={Colors.primary} />
            </View>
        )
    }
    return(
        <GestureRecognizer
            config={{
                velocityThreshold: 0.3,
                directionalOffsetThreshold: 80
            }}
            style={{flex:1}}
            onSwipeLeft={ () => {
                console.log("this is pageNo when sipeLeft: ",pageNo)
                if(totalPage > 1 && pageNo != totalPage) {
                    pageNo += 1;
                    goToNextScreen(text,pageNo,'+')
                }
                // props.navigation.navigate('Next Search', {
                // page: pageNo,
                // searchText: text
                // })
            } }
            onSwipeRight={ () => {
                console.log("this is pageNo when sipeRight: ",pageNo)
                if(pageNo != 1) {
                    pageNo -= 1;
                    goToNextScreen(text,pageNo,'-')
                }
            } }
        >
            <View style={styles.screen} >
                <View style={styles.searchContainer} >
                    <TouchableOpacity style={styles.touchable} onPress={ () => {
                        onSearchClick(text)
                    } } >
                        <Ionicons
                            name="md-search"
                            size={27}
                            color='black' 
                        />
                    </TouchableOpacity>
                    <TextInput
                        style={styles.input}
                        value={text}
                        onChangeText={text => setText(text)}
                    />
                </View>
                <FlatList
                    style={styles.flatList}
                    data={searchedProducts}
                    keyExtractor={(item, index) => item.id.toString() + index} 
                    showsVerticalScrollIndicator={false}
                    renderItem={itemData => <ProductItem
                        image={itemData.item.imageUrl}
                        title={itemData.item.title}
                        price={itemData.item.price}
                        onSelect={ () => {
                            selectItemHandler(itemData.item.id,itemData.item.title)
                        } }
                        onAddToCart={ () => {
                            onAddToCartHandler(itemData.item)
                        } }        
                        >
                            <Button color={Colors.primary} title="Details" onPress={() => {
                                selectItemHandler(itemData.item.id, itemData.item.title)
                            }} />
                            <Button color={Colors.primary} title="To Cart" onPress={() => {
                                onAddToCartHandler(itemData.item)
                            }} />
                        </ProductItem>
                            }
                    ListFooterComponent={getFooter()}   
                />
            </View>
        </GestureRecognizer>
    )
}


const styles = StyleSheet.create({
    screen: {
        margin: 20,
        
    },
    input: {
        padding: 10,
        marginVertical: 8,
        fontSize: 22,
        width: '100%'
        
    },
    searchContainer: {
        borderRadius: 50,
        elevation: 5,
        backgroundColor: '#E8D5D5',
        flexDirection: 'row'
    },
    touchable: {
        padding: 12,
        marginVertical: 8,
    },
    flatList: {
        marginVertical: 20
    }
})

export default SearchScreen;