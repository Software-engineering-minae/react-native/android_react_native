import React, { useEffect, useState } from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import * as orderActions from '../../store/actions/orders';

import OrderItem from '../../components/Shop/OrderItem';
import Colors from '../../constants/Colors';


const OrdersScreen = props => {
    const [isLoading, setIsLoading] = useState(false)
    const dispatch = useDispatch();
    useEffect( ()=> {
        const loadOrders = async () => {
            setIsLoading(true)
            await dispatch(orderActions.retrieveOrders())
            setIsLoading(false)
        }
        loadOrders() 
    }, [dispatch])
    const orders = useSelector(state => state.orders.orders)
    console.log("this is orders in orderScreen: ", orders)
    if (isLoading) {
        return (
            <View style={{alignItems:'center', justifyContent: 'center', flex: 1}} >
                <ActivityIndicator size='large' color={Colors.primary} />
            </View>
        )
    }
    return (
        <FlatList 
            data={orders}
            keyExtractor={item => item.id.toString()}
            renderItem={ itemData => <OrderItem
                total={itemData.item.total}
                date={itemData.item.created_at.split(':')[0].substr(0,9)}
                items={itemData.item.order_items}
                status={itemData.item.status}
            /> }
        />
    )
}

export default OrdersScreen;