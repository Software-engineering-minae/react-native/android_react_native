import React, { useReducer, useCallback } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

import Input from '../../components/UI/Input';
import Colors from '../../constants/Colors';
import Card from '../../components/UI/Card';

const CART_FORM_INPUT_UPDATE = 'CART_FORM_INPUT_UPDATE'; 

const cartFormReducer = (state, action) => {
    switch(action.type){
        case CART_FORM_INPUT_UPDATE:
            const updatedValues = {
                ...state.inputValues,
                [action.input]: action.value
            }
            const updatedValidities = {
                ...state.inputValidities,
                [action.input]: action.isValid
            }
            let updatedFormIsValid = true;
            for (const key in updatedValidities) {
                updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
            }
            return {
                inputValues: updatedValues,
                inputValidities: updatedValidities,
                formIsValid: updatedFormIsValid
            }
        default:
            return state
    }
}


const CartInfoScreen = props => {
    const cartTotalAmount = props.navigation.getParam('total')
    const [cartForm, dispatchCartFormUpdate] = useReducer(cartFormReducer, {
        inputValues: {
            address: '',
            phone_number: ''
        },
        inputValidities: {
            address: false,
            phone_number: false
        },
        formIsvalid: false
    })

    const formInputChangeHandler = useCallback((inputIdentifier, inputValue, inputValidity) => {
        dispatchCartFormUpdate({
            type: CART_FORM_INPUT_UPDATE,
            value: inputValue,
            isValid: inputValidity,
            input: inputIdentifier
        })
    }, [dispatchCartFormUpdate])

    return (
        <View style={styles.screen} >
            <View style={styles.summary} >
                <Text style={styles.summaryText} > Total: <Text style={styles.amount} > ${cartTotalAmount} </Text>
                </Text>
                <Button title="Back" onPress={ () => {
                    props.navigation.goBack()
                } } />
            </View>
            <Card style={styles.card} >
                <View style={styles.inputs} >
                    <Input
                        id="address"
                        label="Address" 
                        required
                        errorText="Please Enter a valid address!"
                        minLength={8}
                        onInputChange={ formInputChangeHandler }
                    />
                    <Input
                        id="phone_number"
                        label="PhoneNumber" 
                        required
                        phoneNumber
                        
                        keyboardType="numeric"
                        errorText="Please Enter a valid phone number!"
                        onInputChange={ formInputChangeHandler }
                    />
                    <View style={styles.button} >
                        <Button title="Order Now" onPress={ () => {} } />
                    </View>
                </View>
            </Card>

        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        margin: 20
    },
    summary: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        elevation: 5,
        marginBottom: 15,
        padding: 5
    },
    card: {
        padding: 20,
        marginVertical: 10
    },
    inputs: {
        marginVertical: 10
    },
    button: {
        alignItems: 'center',
        marginTop: 15,
        padding: 10
    },
    summaryText: {
        fontWeight: 'bold',
        fontSize: 18
    },
    amount: {
        color: Colors.accent
    }
})


export default CartInfoScreen;