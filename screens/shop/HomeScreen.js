import React, { useEffect, useState } from 'react';
import {View, Text, ScrollView, ActivityIndicator} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import ProductType from '../../components/Shop/ProductType';
import CustomHeaderButton from '../../components/UI/HeaderButton';

import * as categoryActions from '../../store/actions/category';
import * as cartActions from '../../store/actions/cart';
import * as authActions from '../../store/actions/auth';
import * as orderActions from '../../store/actions/orders';

import Colors from '../../constants/Colors';

const HomeScreen = props => {
    const [isLoading, setIsLoading] = useState(false);
    const clothe = useSelector(state => state.category.clothe)
    const handicrafts = useSelector(state => state.category.handicrafts)
    const foodstuff = useSelector(state => state.category.foodstuff)
    console.log("FoodStuff is : ", foodstuff)
    const others = useSelector(state => state.category.others)
    const dispatch = useDispatch();
    
    const username = useSelector(state => state.auth.userInfo.username)
    // const x = props.navigation.setParams({
    //     username: username
    // })
    // console.log('X', x)
    // console.log("this is titel: ", props.navigation.getParam("title"))
    // const {navigation} = props
    useEffect(()=>{
        const loadCategory = async () => {
            setIsLoading(true)
            await dispatch(authActions.retriveInfo())
            await dispatch(authActions.retrieveProfile())
            await dispatch(categoryActions.fetchCategory())
            setIsLoading(false)
        }
        loadCategory()

    }, [dispatch])
    if(isLoading) {
        return (
            <View style={{flex:1, justifyContent:'center', alignItems: 'center'}} >
                <ActivityIndicator size="large" color={Colors.primary} />
            </View>
        )
    }
    return(
        <ScrollView>
            <ProductType 
                type={clothe.type}
                image={clothe.image}
                onView={()=>{
                props.navigation.navigate('ProductsOverview', {
                    productsType: clothe.type
                })
            }}
            />
            <ProductType 
                type={handicrafts.type}
                image={handicrafts.image}
                onView={()=>{
                props.navigation.navigate('ProductsOverview', {
                    productsType: handicrafts.type
                })
            }}
            />
            <ProductType 
                type={foodstuff.type}
                image={foodstuff.image}
                onView={()=>{
                props.navigation.navigate('ProductsOverview', {
                    productsType: foodstuff.type 
                })
            }}
            />
            <ProductType 
                type={others.type}
                image={others.image}
                onView={()=>{
                props.navigation.navigate('ProductsOverview', {
                    productsType: others.type 
                })
            }}
            />
        </ScrollView>
        
    )
}

HomeScreen.navigationOptions = navData =>  {
    const title = navData.navigation.getParam('title')
    console.log("this is navdata: ",navData.navigation.state)
    return {
      headerTitle: title,
      headerLeft: ( 
      <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
        <Item 
          title="Menu"
          iconName='md-menu'
          onPress ={  () => {navData.navigation.toggleDrawer()} }
        />
      </HeaderButtons>
      )
    }
  };

export default HomeScreen;