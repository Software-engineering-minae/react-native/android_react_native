import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet, Button, ActivityIndicator, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../../components/UI/HeaderButton';
import ProductItem from '../../components/Shop/ProductItem';
import Colors from '../../constants/Colors';

import * as productActions from '../../store/actions/products';


const UserProductsScreen = props => {
    const userProducts = useSelector(state => state.products.userProducts)
    const cartItems = useSelector(state => state.cart.items)
    const isProducer = useSelector(state => state.auth.userInfo.isProducer)
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    useEffect( () => {
      const loadUserProduct = async () => {
        setIsLoading(true)
        await dispatch(productActions.loadUserProduct())
        setIsLoading(false)
      }
      loadUserProduct()
    },[dispatch])
    
    const editProduct = async productId => {
      await dispatch(productActions.editProduct(productId))
      props.navigation.navigate('EditProduct',{
        edit: true,
        productId: productId
      })
    }

    const deleteProduct = async (productId) => {
      setIsLoading(true)
      await dispatch(productActions.deleteProduct(productId))
      await dispatch(productActions.loadUserProduct())
      setIsLoading(false)
    }

    if ( isLoading ) {
      return (
        <View style={styles.spinner} >
          <ActivityIndicator size='large' />
        </View>
      )
    }

    if(!isProducer) {
      return (
        <View style={{alignItems:'center', justifyContent: 'center'}} >
          <Text style={{fontSize:20,fontWeight: 'bold' }} > This Screen is only available for Producers! </Text>
        </View>
      )
    }

    return (
        <FlatList
            data={userProducts}
            keyExtractor={item => item.id.toString()}
            renderItem={ itemData => <ProductItem
                image={itemData.item.imageUrl}
                title={itemData.item.title}
                price={itemData.item.price}
                onSelect={ () => {} }
                >
                  <Button color={Colors.primary} title="Edit" onPress={() => {
                    editProduct(itemData.item.id)
                  }} />
                  <Button color={Colors.primary} title="Delete" onPress={() => {
                    deleteProduct(itemData.item.id)
                  }} />
              </ProductItem> }
        />
    )
}

UserProductsScreen.navigationOptions = navData => {
    
    return {
        headerTitle: 'Your Products',
        headerLeft: ( 
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
              <Item 
                title="Menu"
                iconName='md-menu'
                onPress ={  () => {navData.navigation.toggleDrawer()} }
              />
            </HeaderButtons>
            ),
        headerRight: (
          <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
            <Item 
              title="Add"
              iconName='md-add'
              onPress ={ () => {
                console.log("thisis navdara",navData)
                navData.navigation.navigate('EditProduct')
              } }
            />
          </HeaderButtons>
        )
        
    }
    
}

const styles = StyleSheet.create({
  spinner: {
    flex:1, 
    alignItems:'center', 
    justifyContent:'center'
  }
})

export default UserProductsScreen;