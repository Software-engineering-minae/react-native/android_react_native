import React, { useReducer, useCallback, useState, useEffect } from 'react';
import {View, Button, Text, TextInput, StyleSheet, KeyboardAvoidingView, Image, ScrollView, TouchableOpacity, Alert} from 'react-native';
import Input from '../../components/UI/Input';
import * as ImagePicker from 'expo-image-picker';
import Card from '../../components/UI/Card';
import UserPermission from '../../class/Permission';
import Colors from '../../constants/Colors';
import { Dropdown } from 'react-native-material-dropdown';
import { useSelector, useDispatch } from 'react-redux';
import * as productActions from '../../store/actions/products';


const UPDATE_PRODUCT_INFO = 'UPDATE_PRODUCT_INFO'; 
const infoFormReducer = (state, action) => {
    switch(action.type) {
        case UPDATE_PRODUCT_INFO: {
            const updatedValues = {
                ...state.inputValues,
                [action.input] : action.value
            
            }
            const updatedValidities = {
                ...state.inputValidities,
                [action.input] : action.isValid
            }
            let updatedFormIsValid = true;
            for (const key in updatedValidities) {
                updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
            }
            return{
                inputValues: updatedValues,
                inputValidities: updatedValidities,
                formIsValid: updatedFormIsValid
            }
        }
        default:
            return state
    }
}

const EditProductScreen = props => {
    const edit = props.route.params?.edit
    const productID = props.route.params?.productId
    const dispatch = useDispatch();
    const selectedProduct = useSelector(state => state.products.selectedProduct)
    console.log("this is selectedProduct: ", selectedProduct)
    const [image, setImage] = useState(edit ? selectedProduct.image : '');
    const dropDownData = [{
        value: 'foodstuff'
    }, {
        value: 'handcrafts'
    }, {
        value: 'clothes'
    }, {
        value: 'others'
    }]
    const [dropdownItem, setDropDownItem] = useState(edit ? selectedProduct.tags[0] : '');


    const [infoFormState, dispatchInfoFormState] = useReducer(infoFormReducer,{
        inputValues: {
            title: edit ? selectedProduct.title : '' ,
            price: edit ? selectedProduct.price : null,
            quantity: edit ? selectedProduct.count : null,
            description: edit ? selectedProduct.description : ''
        },
        inputValidities: {
            title:  edit,
            price: edit,
            quantity: edit,
            description: edit
        },
        formIsValid: edit
    })

    const formChangeHandler = useCallback((inputIdentifier, inputValue, inputValidity)=> {
        dispatchInfoFormState({
            type: UPDATE_PRODUCT_INFO,
            value: inputValue,
            isValid: inputValidity,
            input: inputIdentifier
        })
    }, [dispatchInfoFormState])

    const imagePickerHandler = async () => {
        UserPermission.getCameraPermisson()
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes:ImagePicker.MediaTypeOptions.Images,
            allowsEditing:true,
            aspect:[4,3]
        })
        if(!result.cancelled) {
            setImage(result.uri)
        }
    }

    const onUpdateHandler = async (uri, title, price, quantity, description, tag, id) => {
        await dispatch(productActions.updateProduct(uri, title, price, quantity, description, tag, id))
        await dispatch(productActions.loadUserProduct())
        Alert.alert('Detail of your Product has successfully changed!')
        props.navigation.goBack()
    }

    const onCreateHandler = async (uri, title, price, quantity, description, tag) => {
        await dispatch(productActions.createProduct(uri, title, price, quantity, description, tag))
        await dispatch(productActions.loadUserProduct())
        Alert.alert('Your Product has successfully added!')
        props.navigation.goBack()
    }

    return (
        <KeyboardAvoidingView behavior="height" keyboardVerticalOffset={50} style={styles.screen} >
            <ScrollView style={{width: '100%',height:800}} >
                        <View style={styles.form} >
                            <TouchableOpacity style={styles.touchable} onPress={imagePickerHandler} >
                                {image ? 
                                <Image source={{uri: image}} style={styles.image} />
                                :
                                <View style={{justifyContent: 'center', alignItems: "center", flex: 1}} >
                                    <Text style={{alignSelf: 'center', fontSize: 22, fontWeight: 'bold'}} > No Image found! </Text>
                                    <Text style={{alignSelf: 'center', fontSize: 16, fontWeight: 'bold'}} > Tab here to select one! </Text>
                                </View>
                                }
                            </TouchableOpacity>
                        </View>
                        <Input
                            id="title"
                            label="Title"
                            required
                            errorText="Title cannot be empty!"
                            onInputChange={formChangeHandler}
                            initialValue={infoFormState.inputValues.title}
                        />
                        <Input
                            id="price"
                            label="Price"
                            required
                            keyboardType="numeric"
                            errorText="Price field cannot be empty!"
                            onInputChange={formChangeHandler}
                            initialValue={infoFormState.inputValues.price}
                        />
                        <Input
                            id="quantity"
                            label="Quantity"
                            required
                            keyboardType="numeric"
                            errorText="The least amount is 1!"
                            onInputChange={formChangeHandler}
                            initialValue={infoFormState.inputValues.quantity}
                        />
                        <Input
                            id="description"
                            label="Description"
                            required
                            min={5}
                            errorText="Write at least 5 words for description!"
                            onInputChange={formChangeHandler}
                            initialValue={infoFormState.inputValues.description}
                        />
                        <Dropdown
                            label="Category"
                            data={dropDownData}
                            overlayStyle={dropdownItem == "foodstuff" ? {marginTop: -45} : null}
                            containerStyle={styles.dropdown}
                            value={dropdownItem}
                            onChangeText={value => setDropDownItem(value) }
                        />
                        <View style={{alignItems: 'center', marginVertical: 4,}} >
                            {!edit ? 
                                <Button title="Create" onPress={() => {
                                    onCreateHandler(
                                        image, 
                                        infoFormState.inputValues.title, 
                                        infoFormState.inputValues.price, 
                                        infoFormState.inputValues.quantity, 
                                        infoFormState.inputValues.description, 
                                        dropdownItem
                                    )
                                }} color={Colors.primary} />
                            :
                                <Button title="Submit" onPress={() => {
                                    onUpdateHandler(
                                        image,
                                        infoFormState.inputValues.title, 
                                        infoFormState.inputValues.price, 
                                        infoFormState.inputValues.quantity, 
                                        infoFormState.inputValues.description, 
                                        dropdownItem,
                                        productID
                                    )
                                }} color={Colors.primary} />
                            }
                        </View>
            </ScrollView>
                
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex:1
    },
    form: {
        width: '100%',
        height: 250,
    },
    touchable: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
    },
    imageContainer: {
        width: '100%',
        height: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,        
    },
    image: {
        width: '100%',
        height: '100%'
    },
    inputForm: {
        width: '100%',
        height: '100%'
    },
    dropdown: {
        margin: 5,
        padding: 10,
        borderColor: '#ccc'
    }
})

export default EditProductScreen;