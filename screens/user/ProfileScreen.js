import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import { useSelector, useDispatch } from 'react-redux';

import UserPermission from '../../class/Permission';
import UserProduct from '../../components/UI/UserProduct';

import * as productActions from '../../store/actions/products';
import * as authActions from '../../store/actions/auth';


const ProfileScreen = props => {
    const userProducts = useSelector(state => state.products.userProducts)
    const username = useSelector(state => state.auth.userInfo.username)
    const userImage = useSelector(state => state.auth.userInfo.image)
    const dispatch = useDispatch();
    const [image, setImage] = useState(userImage);
    const [isLoading, setIsLoading] = useState(false);

    useEffect( () => {
        const loadUserProduct = async () => {
          setIsLoading(true)
          await dispatch(productActions.loadUserProduct())
          setIsLoading(false)
        }
        loadUserProduct()
    },[dispatch])

    const editProduct = async (productId,productImage) => {
      await dispatch(productActions.editProduct(productId))
      props.navigation.navigate('EditProduct',{
        edit: true,
        productId: productId,
        productImage: productImage
      })
    }

    const deleteProduct = async (productId) => {
      setIsLoading(true)
      await dispatch(productActions.deleteProduct(productId))
      await dispatch(productActions.loadUserProduct())
      setIsLoading(false)
    }

    const imagePickerHandler = async () => {
        UserPermission.getCameraPermisson()
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes:ImagePicker.MediaTypeOptions.Images,
            allowsEditing:true,
            aspect:[4,3]
        })
        if(!result.cancelled) {
            setImage(result.uri)
        }
    }

    const onAddProductHandler = () => {
        props.navigation.navigate('EditProduct')
    }


    if ( isLoading ) {
        return (
          <View style={styles.spinner} >
            <ActivityIndicator size='large' />
          </View>
        )
    }

    return (
        <LinearGradient colors={['#C9D6FF', '#E2E2E2']} style={styles.gradient} >
            <View style={styles.container} >
                <View style={{alignSelf:'center', marginTop: 3}} >
                    <View style={styles.profileImage} >
                        <Image source={image ? {uri: image} : require('../../assets/profile.jpg')} style={styles.image}  resizeMode='cover' />
                    </View>
                    {/* <TouchableOpacity style={styles.add} onPress={imagePickerHandler} >
                        <MaterialIcons name='add-a-photo' size={30} color="#DFD8C8" style={{marginTop:2, marginLeft:2}} />
                    </TouchableOpacity> */}
                </View>
                <View style={styles.infoContainer} >
                    <Text style={styles.text} > {username} </Text>
                </View>
                <View style={styles.statsContainer} >
                    <Text style={{fontSize: 16, fontWeight: 'normal'}} > {userProducts.length} </Text>
                    <Text style={{fontSize: 12, fontWeight:'bold', color: '#AEB5BC', textTransform:'uppercase'}}> Products </Text>
                </View>
                <FlatList 
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={userProducts}
                    keyExtractor={item => item.id.toString()}
                    renderItem={ itemData => <UserProduct 
                    title={itemData.item.title}
                    price={itemData.item.price}
                    count={itemData.item.count}
                    image={itemData.item.imageUrl}
                    onSelect={ () => {} }
                    onEdit={ () => editProduct(itemData.item.id, itemData.item.imageUrl)}
                    onDelete={ () => deleteProduct(itemData.item.id)}
                    /> }
                />
                <TouchableOpacity style={styles.addButton} onPress={ () => {
                    onAddProductHandler()
                } } >
                    <Text style={{fontSize: 16, fontWeight: 'bold'}} > Add a new Product </Text>
                </TouchableOpacity>

            </View>
        </LinearGradient>
    )
}


const styles = StyleSheet.create({
    container: {
      flex:1,
      alignSelf:'center',
    },
    profileImage: {
      width: 160,
      height: 160, 
      borderRadius: 80,
      overflow:'hidden',
      borderColor:'black',
      borderWidth: 2,
      
    },
    image: {
      flex: 1, 
      width: undefined,
      height:undefined,
    },
    add: {
      backgroundColor: '#414448',
      position: 'absolute',
      bottom: 0,
      right: 0,
      width: 45,
      height: 45,
      borderRadius: 22.5,
      alignItems:'center',
      justifyContent:'center',
      paddingBottom: 3,
      paddingRight: 3
    },
    infoContainer: {
      alignSelf:'center',
      alignItems:'center',
    },
    text: {
      fontSize: 28, 
      fontWeight:"300"
    },
    statsContainer: {
      alignSelf: 'center',
      alignItems:'center',
      flexDirection:'row',
    },
    gradient: {
      flex: 1,
    },
    addButton: {
      alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'center',
      marginTop: 8,
      marginBottom: 6,
      backgroundColor: '#C9D6FF',
      width: 200,
      height: 40,
      borderRadius: 70
    },
    spinner: {
        flex:1, 
        alignItems:'center', 
        justifyContent:'center'
      }
  })

export default ProfileScreen;