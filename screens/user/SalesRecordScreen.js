import React, { useState, useEffect } from 'react';
import  { View, StyleSheet, TouchableOpacity, Text, FlatList, Image, ActivityIndicator   } from 'react-native';
import ProductModal from '../../components/Shop/ProductModal';
import { useDispatch, useSelector } from 'react-redux';
import * as saleActions from '../../store/actions/sale';


const SalesRecordScreen = () => {
    const [isLoading, setIsLoading] = useState(false);
    const salesRecord = useSelector(state => state.sales.soldProduct)
    const dispatch = useDispatch();
    useEffect(()=> {
        const loadSalesRecord = async ()=> {
            setIsLoading(true)
            await dispatch(saleActions.loadSalesRecord())
            setIsLoading(false)
        }
        loadSalesRecord()
    }, [dispatch])

    if (isLoading) {
        return (
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}} >
                <ActivityIndicator size="large" />
            </View>
        )
    }
    
    return (
        <FlatList 
            data={salesRecord}
            keyExtractor={ (item, index) => index.toString() }
            renderItem={ itemData => <ProductModal 
                    items={itemData.item.order_items} 
                    phoneNum={itemData.item.phone_number} 
                    profileImage={itemData.item.profile_image}
                    user={itemData.item.username}
                    count={itemData.item.order_items.length}
                    address={itemData.item.address}
                /> }
        />
    )
    
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 5,
        width: '100%',
        height: 100
    },
    touchable: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between'
    },
    image: {
        height:50,
        width: 50,
        borderRadius: 25
    }
})

export default SalesRecordScreen;