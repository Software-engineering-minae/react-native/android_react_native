import React,{useReducer, useEffect, useCallback, useState} from 'react';
import { View, ScrollView, KeyboardAvoidingView, StyleSheet, Button, ActivityIndicator, Alert, TouchableOpacity, Text } from 'react-native';
import { CheckBox } from 'native-base';
import { LinearGradient } from 'expo-linear-gradient';
//import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import { useDispatch, useSelector } from 'react-redux';
import * as authActions from '../../store/actions/auth';

import Colors from '../../constants/Colors';
import Card from '../../components/UI/Card';
import Input from '../../components/UI/Input';
//import Headerbutton from '../../components/UI/HeaderButton';


const LOGIN_FORM_INPUT_UPDATE = "LOGIN_FORM_INPUT_UPDATE"; 
const SIGNUP_FORM_INPUT_UPDATE = "SIGNUP_FORM_INPUT_UPDATE"; 
const loginFormReducer = (state,action) => {
    switch(action.type){
        case LOGIN_FORM_INPUT_UPDATE:
            const updatedValues = {
                ...state.inputValues,
                [action.input] : action.value
            }
            const updatedValidities = {
                ...state.inputValidities,
                [action.input] : action.isValid
            }
            let updatedFormIsValid = true
            for(const key in updatedValidities){
                updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
            }
            return {
                inputValues: updatedValues,
                inputValidities: updatedValidities,
                formIsValid: updatedFormIsValid
            }
        default:
            return state
    }
}

const signupFormReducer = (state, action) => {
    switch(action.type){
        case SIGNUP_FORM_INPUT_UPDATE:
            const updatedValues = {
                ...state.inputValues,
                [action.input] : action.value
            
            }
            const updatedValidities = {
                ...state.inputValidities,
                [action.input] : action.isValid
            }
            let updatedFormIsValid = true;
            for (const key in updatedValidities) {
                updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
            }

            return{
                inputValues: updatedValues,
                inputValidities: updatedValidities,
                formIsValid: updatedFormIsValid
            }
        default:
            return state
    }
}


const AuthScreen = props => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const [isChecked, setIsChecked] = useState(false);
    const [isSignup, setIsSignup] = useState(false);
    const token = useSelector(state => state.auth.token)
    const [loginFormState, dispatchLoginFormState] = useReducer(loginFormReducer, {
        inputValues: {
            username: '',
            password: ''
        },
        inputValidities: {
            username: false,
            password: false
        },
        formIsValid: false
    })
    useEffect(()=>{
        if (error){
            Alert.alert('An error occured', error, [{text:'Okay'}])
        }
    }, [error])
    
    const [signupFormState, dispatchSignupFormState] = useReducer(signupFormReducer, {
        inputValues: {
            firstname: '',
            lastname: '',
            username: '',
            email:'',
            password:'',
            phone_number:''
        },
        inputValidities: {
            firstname: false,
            lastname: false,
            username: false,
            email: false,
            password: false,
            phone_number: false

        },
        formIsValid : false
    })
    

    const authInputChangeHandler = useCallback((inputIdentifier, inputValue, inputValidity) => {
        if (isSignup){
            dispatchSignupFormState({
                type: SIGNUP_FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier
            })
        }
        else{
            dispatchLoginFormState({
                type: LOGIN_FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier 
            })
        }  
    }, [isSignup ? dispatchSignupFormState : dispatchLoginFormState])

    const authHandler = async () => {
        let action;
        if (isSignup){
        action = authActions.signup(
            signupFormState.inputValues.firstname,
            signupFormState.inputValues.lastname,
            signupFormState.inputValues.username,
            signupFormState.inputValues.password,
            signupFormState.inputValues.phone_number,
            signupFormState.inputValues.email,
            isChecked
            )
            
        }
        else {
        action = authActions.login(
            loginFormState.inputValues.username,
            loginFormState.inputValues.password
            )
        }
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(action);
            if(isSignup){
                setIsLoading(false)
            }
        }
        catch (err) {
            setError(err.message);
            setIsLoading(false);
            }
            
    }

    return(
        <KeyboardAvoidingView behavior="height" keyboardVerticalOffset={20} style={styles.screen} >
            <LinearGradient colors={['#F3BAE3', '#9CBAED']} style={styles.gradient} >
                <Card style={styles.authContainer} >
                    {!isSignup 
                     && 
                    <ScrollView>
                        <Input 
                            id="username" 
                            label="Username" 
                            keyboardType="default"
                            required
                            autoCapitalize='none'
                            errorText="Please enter a valid username"
                            onInputChange={ authInputChangeHandler }
                            initialValue=""
                        />
                        <Input 
                            id="password" 
                            label="Password" 
                            keyboardType="default"
                            secureTextEntry
                            required
                            minLength={5}
                            autoCapitalize='none'
                            errorText="Please enter a valid password address"
                            onInputChange={ authInputChangeHandler }
                            initialValue=""
                        />
                    </ScrollView>
                    }
                    {isSignup &&
                    <ScrollView>
                            <Input 
                                 id="firstname" 
                                 label="Firstname" 
                                 keyboardType="default"
                                 required
                                 autoCapitalize='none'
                                 onInputChange={ authInputChangeHandler }
                                 initialValue=""
                            />
                            <Input 
                                 id="lastname" 
                                 label="Lastname" 
                                 keyboardType="default"
                                 required
                                 autoCapitalize='none'
                                 onInputChange={ authInputChangeHandler }
                                 initialValue=""
                            />
                            <Input 
                                 id="username" 
                                 label="Username" 
                                 keyboardType="default"
                                 required
                                 minLength={6}
                                 autoCapitalize='none'
                                 errorText="Please enter a valid username"
                                 onInputChange={ authInputChangeHandler }
                                 initialValue=""
                            />
                            <Input 
                                id="password" 
                                label="Password" 
                                keyboardType="default"
                                secureTextEntry
                                required
                                minLength={5}
                                autoCapitalize='none'
                                errorText="Please enter a valid password "
                                onInputChange={ authInputChangeHandler }
                                initialValue=""
                            />
                            <Input 
                                id='phone_number' 
                                label="Phone Number" 
                                keyboardType="numeric"
                                autoCapitalize='none'
                                required
                                phoneNumber
                                errorText="Please enter a valid phone number"
                                onInputChange={ authInputChangeHandler }
                                initialValue=""
                            />
                            <Input 
                                id='email' 
                                label="E-Mail" 
                                keyboardType="email-address"
                                autoCapitalize='none'
                                required
                                errorText="Please enter a valid email address"
                                onInputChange={ authInputChangeHandler }
                                initialValue=""
                            />
                            <View style={styles.touchable} >
                                <TouchableOpacity onPress={() => {
                                        setIsChecked(!isChecked)
                                        console.log("clicked: ", isChecked)
                                        }} >
                                        <CheckBox
                                            checked={isChecked}
                                            onPress={() => {
                                                setIsChecked(!isChecked)
                                                console.log("clicked: ", isChecked)
                                                }}
                                        />
                                </TouchableOpacity>
                                <Text style={styles.text} > I am a Producer </Text>
                            </View>
                            
                            
                            
                    </ScrollView>
                    }
                    <View style={styles.buttonContainer} >
                        {isLoading ? 
                            (<ActivityIndicator size='small' color={Colors.primary} />) 
                            : 
                            (<Button title={isSignup ? "Sign Up" : "Login"} color={Colors.primary} onPress={ authHandler } />)
                        }
                    </View>
                    <View style={styles.buttonContainer} >
                        <Button title={ `Switch to ${isSignup ? "Login": "Sign Up"}` } color={Colors.accent} onPress={ () => {
                                setIsSignup((prevState) => !prevState)
                        } } />
                    </View>
                </Card>
            </LinearGradient>
        </KeyboardAvoidingView>
    )
}


const styles = StyleSheet.create({
    screen: {
        flex:1
    },
    authContainer: {
        width:'80%',
        maxWidth: 600,
        maxHeight: 600,
        padding: 20

    },
    gradient: {
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    buttonContainer: {
        marginTop: 10
    },
    touchable: {
        flexDirection: 'row',
        marginVertical: 5
    },
    text: {
        fontSize: 14,
        fontWeight: 'bold',
        marginHorizontal: 10,
        paddingHorizontal: 5
    }
})

export default AuthScreen;