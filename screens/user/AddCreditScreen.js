import React, { useState } from 'react';
import { View, StyleSheet, FlatList, Text, TouchableOpacity  } from 'react-native';
import { Fumi } from 'react-native-textinput-effects';
import { Ionicons } from '@expo/vector-icons';
import { useDispatch } from 'react-redux';

import * as authActions from '../../store/actions/auth';


const AddCreditScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const [credit, setCredit] = useState(0);
    const defaultData = [
        5000,
        10000,
        15000,
        20000,
        30000,
        50000
    ]

    const addCredit = async (credit) => {
        await dispatch(authActions.addCredit(credit))
        await dispatch(authActions.retrieveProfile())
        navigation.navigate('Products', {
            screen: 'Home'
        });
    }

    const getHeader = () => {
        return (
            <Fumi 
                label={'Credit'}
                iconClass={Ionicons}
                iconName={"md-cash"}
                iconSize={25}
                value={credit.toString()}
                onChangeText={ text => setCredit(text) }
                keyboardType={'number-pad'}
                style={{margin:10, backgroundColor: 'lightblue', borderRadius: 10}}
            />
        )
    }

    const getFooter = () => {
        return (
            <TouchableOpacity style={styles.footerTouchable}  onPress={ () => addCredit(credit) } >
                <Text style={{color: 'white'}} > Submit </Text>
            </TouchableOpacity>
        )
    }

    return (
        <FlatList
            data={defaultData}
            keyExtractor={ (item,idx) => item.toString() + idx }
            columnWrapperStyle={{alignSelf: 'center'}}
            renderItem={itemData => <TouchableOpacity style={styles.touchable} onPress={ () => setCredit(itemData.item) } >
                <Text style={{alignSelf: 'center'}} >{itemData.item}$</Text>
            </TouchableOpacity>}
            numColumns={3}
            ListHeaderComponent={ getHeader() } 
            ListFooterComponent={ getFooter() }
            ListFooterComponentStyle={{alignSelf:'center'}}
        />
    )
}

const styles = StyleSheet.create({
    touchable: {
        margin: 10, 
        justifyContent: 'space-evenly', 
        backgroundColor:'lightyellow', 
        width:60, 
        height: 30,
        borderRadius: 20
    },
    footerTouchable: {
        marginVertical: 10, 
        width: 60, 
        height: 30, 
        borderRadius:45, 
        backgroundColor: 'lightgreen', 
        alignItems:'center', 
        justifyContent:'center'
    }
})

export default AddCreditScreen;