import React, {useEffect, useState} from 'react';
import { StyleSheet, View, TouchableOpacity, Image, ScrollView, Alert, RefreshControl } from 'react-native';
import { MaterialIcons, Octicons } from '@expo/vector-icons';
import { Fumi } from 'react-native-textinput-effects';
import * as ImagePicker from 'expo-image-picker';
import { LinearGradient } from 'expo-linear-gradient';
import { useSelector, useDispatch } from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import CustomHeaderButton from '../../components/UI/HeaderButton';

import * as authActions from '../../store/actions/auth';

import UserPermission from '../../class/Permission';
import { FlatList } from 'react-native-gesture-handler';

const EditInfoScreen = props => {
    const userInfo = useSelector(state => state.auth.userInfo)
    const dispatch = useDispatch();
    const [enable, setEnable] = useState(false);
    const [isChanged, setIsChanged] = useState(false);
    const [imageChanged, setImageChanged] = useState(false);
    const [image, setImage] = useState(userInfo.image);
    const [name, setName] = useState(userInfo.firstName);
    const [lastname, setLastname] = useState(userInfo.lastName);
    const [email, setEmail] = useState(userInfo.email);
    const [username, setUsername] = useState(userInfo.username);
    const [phonenumber, setPhonenumber] = useState(userInfo.phoneNumber);
    const [password, setPassword] = useState('');
    const [Cpassword, setCpassword] = useState('');
    const [passChange, setPassChange] = useState(false);

    const onSubmitHandler = async (image, name, lastname, email, username, phonenumber, password, cpassword, passChange, imageChanged) => {
        let valid = true;
        if (username.length < 5) {
        valid = false
        return (
            Alert.alert('Error on Username','Username Field must be more than 4 words!', [{
            text: 'Got it!'
            }])
        )

        }
        if (phonenumber.length != 11) {
        valid = false
        return (
            Alert.alert('Error on Phonenumber','Phonenumber Field must contain exactly 11 numbers!', [{
            text: 'Got it!'
            }])

        )
        }
        if (passChange) {
        if (password.length < 5) {
            valid = false
            return (
            Alert.alert('Error on Password','Password Field must be more than 4 words!', [{
                text: 'Got it!'
            }])
            )
        }
        if (password != cpassword) {
            valid = false
            return (
            Alert.alert('Error on Password','Password and Confirm Password must match!', [{
                text: 'Got it!'
            }])
            )
        }
        }
        const data = {
            image: image,
            name : name,
            lastname: lastname,
            email: email,
            username: username,
            phonenumber: phonenumber,
            password: password
        }
        console.log("this is data: ",data)
        await dispatch(authActions.editUserInfo(data, passChange, imageChanged))
    }

    const imagePickerHandler = async () => {
        UserPermission.getCameraPermisson()
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes:ImagePicker.MediaTypeOptions.Images,
            allowsEditing:true,
            aspect:[4,3]
        })
        if(!result.cancelled) {
            setImage(result.uri)
            setImageChanged(true)
            setIsChanged(true)
        }
    }
    const {navigation} = props;
    // useEffect(()=> {
    //     navigation.setOptions({headerRight: () => {
    //         <HeaderButtons HeaderButtonComponent={CustomHeaderButton} >
    //             <Item 
    //                 title="Add"
    //                 iconName='md-add'
    //             />
    //         </HeaderButtons>
    //     }})

    // },[navigation])

    
    return (
        <LinearGradient colors={['#C9D6FF', '#E2E2E2']} style={styles.gradient} >
            <View style={{flex: 1}}>
                <ScrollView style={styles.container} showsVerticalScrollIndicator={false} >
                <View style={{alignSelf:'center', marginBottom: 15}} >
                    <View style={styles.profileImage} >
                        <Image source={image ? {uri: image} : require('../../assets/profile.jpg')} style={styles.image}  resizeMode='cover' />
                    </View>
                    <TouchableOpacity style={styles.add} onPress={imagePickerHandler} >
                        <MaterialIcons name='add-a-photo' size={30} color="#DFD8C8" style={{marginTop:2, marginLeft:2}} />
                    </TouchableOpacity>
                </View>
                <Fumi 
                    label={'Name'}
                    labelStyle={{color: '#808285'}}
                    passiveIconColor={'#808285'}
                    iconColor={'#5199FF'}
                    iconClass={MaterialIcons}
                    iconName={'edit'}
                    inputPadding={16}
                    iconSize={20}
                    iconWidth={35}
                    style={{ width: 320, borderRadius: 15, marginBottom: 5 }}
                    value={name}
                    onChangeText={text => { 
                    setName(text)
                    setIsChanged(true)
                    }}
                />
                <Fumi 
                    label={'Lastname'}
                    labelStyle={{color: '#808285'}}
                    passiveIconColor={'#808285'}
                    iconColor={'#5199FF'}
                    iconClass={MaterialIcons}
                    iconName={'edit'}
                    inputPadding={16}
                    iconSize={20}
                    iconWidth={35}
                    style={{ width: 320, borderRadius: 15, marginBottom: 5 }}
                    value={lastname}
                    onChangeText={text => { 
                    setLastname(text)
                    setIsChanged(true)
                    }}
                />
                <Fumi 
                    label={'E-mail'}
                    labelStyle={{color: '#808285'}}
                    passiveIconColor={'#808285'}
                    iconColor={'#5199FF'}
                    iconClass={MaterialIcons}
                    iconName={'email'}
                    inputPadding={16}
                    iconSize={20}
                    iconWidth={35}
                    style={{ width: 320, borderRadius: 15, marginBottom: 5 }}
                    value={email}
                    onChangeText={text => { 
                    setEmail(text)
                    setIsChanged(true)
                    }}
                />
                <Fumi 
                    label={'Username'}
                    labelStyle={{color: '#808285'}}
                    passiveIconColor={'#808285'}
                    iconColor={'#5199FF'}
                    iconClass={MaterialIcons}
                    iconName={'edit'}
                    inputPadding={16}
                    iconSize={20}
                    iconWidth={35}
                    style={{ width: 320, borderRadius: 15, marginBottom: 5 }}
                    value={username}
                    onChangeText={text => { 
                    setUsername(text)
                    setIsChanged(true)
                    }}
                />
                <Fumi 
                    label={'Phonenumber'}
                    labelStyle={{color: '#808285'}}
                    passiveIconColor={'#808285'}
                    iconColor={'#5199FF'}
                    iconClass={MaterialIcons}
                    iconName={'phone-iphone'}
                    inputPadding={16}
                    iconSize={20}
                    iconWidth={35}
                    style={{ width: 320, borderRadius: 15, marginBottom: 5, color: 'blue' }}
                    value={phonenumber}
                    onChangeText={text => { 
                    setPhonenumber(text)
                    setIsChanged(true)
                    }}
                />
                <Fumi 
                    label={'Password'}
                    labelStyle={{color: '#808285'}}
                    passiveIconColor={'#808285'}
                    iconColor={'#5199FF'}
                    iconClass={Octicons}
                    iconName={'key'}
                    inputPadding={16}
                    iconSize={20}
                    iconWidth={35}
                    style={{ width: 320, borderRadius: 15, marginBottom: 5 }}
                    onChangeText={text => { 
                    if(text.length != 0){
                        setPassChange(true)

                    }
                    else {
                        setPassChange(false)
                    }
                    setPassword(text)
                    setIsChanged(true)
                    }}
                    secureTextEntry
                />
                <Fumi 
                    label={'Confirm Password'}
                    labelStyle={{color: '#808285'}}
                    passiveIconColor={'#808285'}
                    iconColor={'#5199FF'}
                    iconClass={Octicons}
                    iconName={'key'}
                    inputPadding={16}
                    iconSize={20}
                    iconWidth={35}
                    style={{ width: 320, borderRadius: 15, marginBottom: 5 }}
                    onChangeText={text => { 
                    setCpassword(text)
                    setIsChanged(true)
                    setPassChange(true)
                    }}
                />
                <TouchableOpacity disabled={!isChanged} onPress={() => {
                    onSubmitHandler(image, name, lastname, email, username, phonenumber, password, Cpassword, passChange, imageChanged)
                }} >
                    <Image source={require('../../assets/profile.jpg')} style={{height: 50, width: 50, alignSelf: 'center', opacity:!isChanged ? 0.2 : 1, borderRadius: 25}} />
                </TouchableOpacity>
                </ScrollView>

            </View>
        </LinearGradient>
        
    );
}

const styles = StyleSheet.create({
    gradient: {
        flex: 1,
      },
    container: {
        flex:1,
        alignSelf:'center',
        margin: 20,
      },
      profileImage: {
        width: 160,
        height: 160, 
        borderRadius: 80,
        overflow:'hidden',
        borderColor:'black',
        borderWidth: 2
      },
      image: {
        flex: 1, 
        width: undefined,
        height:undefined,
      },
      add: {
        backgroundColor: '#414448',
        position: 'absolute',
        bottom: 0,
        right: 0,
        width: 45,
        height: 45,
        borderRadius: 22.5,
        alignItems:'center',
        justifyContent:'center',
        paddingBottom: 3,
        paddingRight: 3
      },
      address: {
        marginVertical: 15
      }
})

export default EditInfoScreen;