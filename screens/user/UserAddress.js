import React, { useState } from 'react';
import {FlatList, StyleSheet, TouchableOpacity, Text, View, ActivityIndicator} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';
import { LinearGradient } from 'expo-linear-gradient';
import Dialog from 'react-native-dialog';
import Colors from '../../constants/Colors';

import Address from '../../components/UI/Address';
import * as authActions from '../../store/actions/auth';


const UserAddress = props => {
    const Addresses = useSelector(state => state.auth.Address)
    const [visible, setVisible] = useState(false);
    const [province, setProvince] = useState('');
    const [city, setCity] = useState('');
    const [address, setAddress] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const dispatch = useDispatch();

    const onAddAddressHandler = async (province, city, address) => {
        await dispatch(authActions.addAddress(province,city,address));
    }

    const onDeleteAddressHandler = async (addressId) => {
        setIsLoading(true)
        await dispatch(authActions.deleteAddress(addressId));
        setIsLoading(false)
    }
    if (isLoading) {
        return (
            <LinearGradient colors={['#C9D6FF', '#E2E2E2']} style={styles.gradient} >
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                    <ActivityIndicator size='large' color={Colors.primary} />
                </View>
            </LinearGradient>
        )
    }

    return (
        <LinearGradient colors={['#C9D6FF', '#E2E2E2']} style={styles.gradient} >
            <FlatList  
                data={Addresses}
                style={{paddingBottom:10}}
                keyExtractor={item => item.id.toString()}
                renderItem={ itemData => <Address  
                    index={itemData.index + 1}
                    province={itemData.item.province}
                    city={itemData.item.city}
                    address={itemData.item.address}
                    onDelete={ () => {
                        onDeleteAddressHandler(itemData.item.id)
                    } }
                /> }
            />
            <TouchableOpacity style={styles.touchable} onPress={() => {
                setProvince('')
                setCity('')
                setAddress('')
                setVisible(true)
            }} >
              <MaterialIcons name="add-location" size={25} style={{ paddingVertical: 2}} />
              <Text style={{fontSize: 16, paddingVertical: 2, alignSelf: 'center'}}>New Address</Text>
            </TouchableOpacity>
            <Dialog.Container visible={visible} >
                <Dialog.Title> New Address </Dialog.Title>
                <Dialog.Input placeholder='Enter your Province' onChangeText={text => setProvince(text)} value={province} />
                <Dialog.Input placeholder='Enter your City' onChangeText={text => setCity(text)} value={city} />
                <Dialog.Input placeholder='Enter your Address' onChangeText={text => setAddress(text)} value={address} />
                <Dialog.Button label="Cancel" style={{color:'red'}} onPress={() => setVisible(false)} />
                <Dialog.Button label="Add" onPress={() => {
                    onAddAddressHandler(province,city,address)
                    setVisible(false)
                }} />
            </Dialog.Container>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    gradient: {
        flex: 1
    },
    touchable: {
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: '#C9D6FF',
        flexDirection: 'row', 
        height: 30, 
        width: 130,  
        marginVertical: 10, 
        borderRadius: 15,
    }
}) 

export default UserAddress;