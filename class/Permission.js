import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';


class UserPermisson {

    getCameraPermisson = async () => {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
        if ( status != "granted" ){
            alert("we need permisson to use your camera roll")
        }
    }

}

export default new UserPermisson();