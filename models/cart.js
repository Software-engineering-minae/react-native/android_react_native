class CartItem {
    constructor(quantity, productImage, productPrice, productTitle, productDescription, sum) {
        this.quantity = quantity,
        this.productImage = productImage,
        this.productPrice = productPrice,
        this.productTitle = productTitle,
        this.productDescription = productDescription,
        this.sum = sum
    }
}

export default CartItem;