class Product {
  constructor(id, ownerId, title, imageUrl, description, price, type, hashtags) {
    this.id = id;
    this.ownerId = ownerId;
    this.imageUrl = imageUrl;
    this.title = title;
    this.description = description;
    this.price = price;
    this.type = type;
    this.hashtags = hashtags;
  }
}

export default Product;
