class Comment {
    constructor(id, like, dislike, commenter, comment) {
        this.id = id,
        this.like = like,
        this.dislike = dislike,
        this.commenter = commenter,
        this.comment = comment
    }
  }
  
  export default Comment;
  