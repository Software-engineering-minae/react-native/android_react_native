class UserProduct {
    constructor(id, ownerId, title, imageUrl, description, price, type, count) {
      this.id = id;
      this.ownerId = ownerId;
      this.imageUrl = imageUrl;
      this.title = title;
      this.description = description;
      this.price = price;
      this.type = type;
      this.count = count
    }
  }
  
  export default UserProduct;
  