import React from 'react';
import { View, Image, StyleSheet, TouchableNativeFeedback, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

const UserProduct = props => {
    let source;
    if (props.image){
        source={uri: props.image}
    }
    else {
        source = require('../../assets/profile.jpg')
    }
    return(
        <View style={styles.product} >
            <View style={styles.touchable} >
                <TouchableNativeFeedback useForeground onPress={props.onSelect} >
                    <View>
                        <View style={styles.imageContainer} >
                            <Image source={source} style={styles.image} />
                        </View>
                        <View style={styles.title} >
                            <Text style={{fontWeight: 'bold', fontSize: 18, textTransform: 'capitalize'}}>{props.title}</Text>
                        </View>
                        <View style={styles.text}  >
                            <Text style={{fontWeight:'400', marginHorizontal: 10, fontSize: 16}}>{props.price}$</Text>
                            <Text style={{fontWeight:'400', marginHorizontal: 10, fontSize: 16}}>{props.count} Available</Text>
                        </View>
                    </View>
                </TouchableNativeFeedback>
                <View style={styles.actions} >
                    <TouchableNativeFeedback  >
                        <MaterialIcons name="edit" size={20} color="blue" onPress={ props.onEdit } />
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback>
                        <MaterialIcons name="delete" size={20} color="red" onPress={ props.onDelete } /> 
                    </TouchableNativeFeedback>
                </View>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    product: {
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white',
        height: 300,
        margin: 10,
        width: 250,
    },
    touchable: {
        borderRadius: 10,
        overflow:'hidden'
    },
    imageContainer: {
        width:'100%',
        height: '66%',
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10,
        overflow:'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    title: {
        alignItems:'center',
        height: '5%',
        padding: 8,
        marginTop: 2,
        marginBottom: 15
    },
    text: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '10%',
    },
    actions: {
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '10%',
        paddingHorizontal: 20,
        paddingBottom: 20
    }
})


export default UserProduct;