import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Ionicons} from '@expo/vector-icons';

const Address = props => {
    return (
          <View style={styles.container} >
            <View style={styles.textContainer} >
                <View style={{flexDirection: 'row'}} >
                    <Text style={{fontSize: 18, paddingBottom: 5, marginBottom: 4}} > {props.index}-</Text>
                    <Text style={{fontSize: 16, paddingTop: 1}} >{props.province}/{props.city}/{props.address}</Text>
                </View>
                { props.onDelete ? 
                    <TouchableOpacity style={{alignSelf: 'flex-end', marginBottom: 8}} >
                        <Ionicons name="md-trash" color='red' size={25} onPress={props.onDelete} />
                    </TouchableOpacity>
                :
                    <View>
                        
                    </View>
                 }
            </View>
          </View>
    )
}

const styles = StyleSheet.create({
    gradient: {
        flex: 1
    },
    container: {
        height: 50, 
        width: 320, 
        elevation: 5, 
        marginHorizontal: 20, 
        marginTop: 25, 
        backgroundColor:'#C9D6FF', 
        borderRadius: 25,
        marginBottom: 10
    },
    textContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly', 
        marginTop: 12, 
        marginLeft: 5
    }
})

export default Address;