import React, { useReducer, useEffect } from 'react';
import { View, TextInput, StyleSheet, Text } from 'react-native';

const INPUT_CHANGE = "INPUT_CHANGE";
const INPUT_BLUR = "INPUT_BLUR";
const inputReducer = (state, action) => {
    switch(action.type) {
        case INPUT_CHANGE:
            return {
                ...state,
                value: action.value,
                isValid: action.isValid
            }
        case INPUT_BLUR:
            return {
                ...state,
                touched: true
            }
        default:
            return state
    }
}


const Input = props => {

    const [inputState, dispatch] = useReducer(inputReducer, {
        value: props.initialValue ? props.initialValue : '' ,
        isValid: props.initiallyValid ? props.initiallyValid : '' ,
        touched: false
    })

    const { onInputChange, id } = props
    useEffect(()=> {
        if(inputState.touched){
            onInputChange(id, inputState.value, inputState.isValid)
        }
    }, [onInputChange, inputState, id])

    const textChangeHandler = text => {
        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const usernameRegex = /^[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*$/
        const phoneNumberRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/
        let isValid = true;
        if (props.required && text.trim().length === 0) {
        isValid = false;
        }
        if (props.email && !emailRegex.test(text.toLowerCase())) {
        isValid = false;
        }
        if (props.min != null && +text < props.min) {
        isValid = false;
        }
        if (props.max != null && +text > props.max) {
        isValid = false;
        }
        if (props.minLength != null && text.length < props.minLength) {
        isValid = false;
        }
        // if(props.username && !usernameRegex.test(text)) {
        //     isValid = false
        // }
        if (props.phoneNumber && phoneNumberRegex.test(parseInt(text)) && text.length != 11) {
            isValid = false
        }

        dispatch({
            type: INPUT_CHANGE,
            value: text,
            isValid: isValid
        })
    }
    const loseFocusHandler = () => {
        dispatch({
            type:INPUT_BLUR
        })
    }

    return (
        <View style={styles.formControl} >
            <Text style={styles.label}> {props.label} </Text>
            <TextInput
                {...props}
                style={styles.textInput} 
                value={inputState.value}
                onChangeText={ textChangeHandler }
                onBlur={ loseFocusHandler }
            />
            {!inputState.isValid && inputState.touched &&
                <View style={styles.errorContainer} >
                    <Text style={styles.errorText}>{props.errorText}</Text>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    formControl: {
        width:'100%'
    },
    label: {
        fontWeight: 'bold',
        marginVertical: 10
    },
    textInput: {
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#CCC',
        borderBottomWidth: 1
    },
    errorContainer: {
        marginVertical: 5
    },
    errorText: {
        color:'red',
        fontWeight: 'bold',
        fontSize : 13,
    }
})

export default Input;