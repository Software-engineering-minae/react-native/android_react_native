import React from 'react';
import {View, Text, TouchableNativeFeedback, StyleSheet} from 'react-native';
import {AntDesign} from '@expo/vector-icons';

const ProductDetail = props => {
    return (
        <View style={styles.commentContainer} >
            <View style={styles.commentStyle} >
                <Text>
                    {props.like}
                </Text>
                <TouchableNativeFeedback onPress={props.likeHandler} >
                    <AntDesign name="like1" size={30} color="green" />
                </TouchableNativeFeedback>
                <Text>
                    {props.dislike}
                </Text>
                <TouchableNativeFeedback onPress={props.dislikeHandler} >
                    <AntDesign name="dislike1" size={30} color="red" />
                </TouchableNativeFeedback>
                <Text style={styles.commenter}> {props.commenter} </Text>
            </View>
            <View style={styles.comment} >
                <View style={{height:'100%', width:'100%'}} >
                    <Text style={styles.commentText} > {props.comment} </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    commentContainer: {
        width: '100%',
        height: 150,
        elevation: 10,
        borderRadius: 10,
        marginVertical: 20,
        backgroundColor: '#f5e6e3',
        
        
    },
    commentStyle: {
        flexDirection: "row-reverse",
        justifyContent: 'space-around',
        margin: 15,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden'
    },
    comment: {
        width: '95%',
        height:'50%',
        borderWidth:0.5,
        borderColor:'#ffe1dc',
        borderRadius: 15,
        padding: 10,
        marginHorizontal: 10

    },
    commentText: {
        padding: 8,
        fontSize: 14,
        fontWeight: 'bold'
    },
    commenter: {
        fontSize: 18,
        fontWeight: 'bold'
    }
})

export default ProductDetail;