import React from 'react';
import {View, Text, Image,  TouchableNativeFeedback, StyleSheet} from 'react-native';


const ProductType = props => {
    return(
        <View style={styles.pType} >
            <View style={styles.touchable} >
                <TouchableNativeFeedback onPress={props.onView} useForeground >
                    <View>
                        <View style={styles.imageContainer} >
                            <Image style={styles.image} source={{uri:props.image}} />
                        </View>
                        <View style={styles.textContainer} >
                            <Text style={styles.text}>{props.type}</Text>
                        </View>
                    </View>
                </TouchableNativeFeedback>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    pType: {
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white',
        height: 300,
        margin: 20
    },
    touchable:{
        overflow: 'hidden',
        borderRadius: 10
    },
    imageContainer:{
        width: '100%',
        height: '80%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden'
    },
    image:{
        height:'100%',
        width: '100%'
    },
    textContainer: {
        width: '100%',
        height: '20%',
        alignItems: 'center',
        padding: 20,
        backgroundColor: 'black'
    },
    text:{
        fontSize: 20,
        fontWeight: 'bold',
        color:'white'
    }
})

export default ProductType;