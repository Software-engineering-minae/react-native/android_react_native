import React, {useState} from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, FlatList, ActivityIndicator } from 'react-native';
import Modal from 'react-native-modal';
import ModalDropdown from 'react-native-modal-dropdown';
import { Ionicons, MaterialIcons, Entypo } from '@expo/vector-icons';
import { useDispatch } from 'react-redux';

import * as saleActions from '../../store/actions/sale';

const ProductModal = ({ user, count, phoneNum, address, profileImage, items}) => {
    const [isVisible, setIsVisible] = useState(false);
    const [selectedStatus, setSelectedStatus] = useState(items[0].status);
    const [isLoading, setIsLoading] = useState(false);
    const [showUserInfo, setShowUserInfo] = useState(false); 
    const dispatch = useDispatch();

    const onSelectStatus = async (idx,value, items) => {
        let index = idx;
        let orderItemsId = []
        for (const key in items){
            orderItemsId.push(items[key].id)
        }
       if ( value == "Deliverd") {
           setSelectedStatus(true)
           setIsLoading(true)
           await dispatch(saleActions.updateStatus(orderItemsId, 1))
           setIsLoading(false)
       }
       else {
            setSelectedStatus(false)
            setIsLoading(true)
            await dispatch(saleActions.updateStatus(orderItemsId, 0))
            setIsLoading(false)
       }
    }
    if ( isLoading ) {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                <ActivityIndicator size='large'  />
            </View>
        )
    }

    const getHeader = () => {
        return (
            <View style={{flexDirection:'row', alignSelf: 'center'}} >
                <View style={{marginHorizontal:23}} >
                    <Text> Count </Text>
                </View>
                <View style={{marginHorizontal: 6}} >
                    <Text> Price </Text>
                </View>
            </View>
        )
    }
    
    return (
        <View>
            <View style={styles.container} >
                    <TouchableOpacity style={styles.touchable} onPress={()=> {setShowUserInfo(true)}}  >
                        <Image source={require('../../assets/profile.jpg')} style={styles.image2} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchable} onPress={ () => { setIsVisible(true) } } >
                        <Text style={{marginTop: 20}}> User {user} purchased {count} products! </Text>
                    </TouchableOpacity>
                    <ModalDropdown onSelect={ (idx, value) => onSelectStatus(idx,value,items) } options={['Sending', 'Deliverd']}  style={{marginTop: 30, marginRight: 10, flexDirection: 'column'}} dropdownStyle={{borderColor:'blue', paddingBottom: 20, flexDirection: 'row'}} dropdownTextStyle={{fontSize:20}} >
                        <Ionicons name="md-arrow-dropdown" color="red" size={24} style={{marginLeft: 5}} />
                        <Text style={{paddingRight: 5}} >{ selectedStatus ? "Deliverd" : "Sending"}</Text>
                    </ModalDropdown>
            </View>
            <View>
                <Modal isVisible={isVisible} style={styles.modal1} backdropOpacity={0.2}  >
                    <View style={{height: 20, width:25}} >
                        <TouchableOpacity onPress={() => setIsVisible(false)} >
                            <Entypo name="circle-with-cross" size={24} color="black" />
                        </TouchableOpacity>
                    </View>
                        <FlatList 
                            data={items}
                            keyExtractor={ item => item.id.toString() }
                            renderItem={ itemData => (
                                <View style={{flexDirection: 'row'}} >
                                    <View style={{marginTop:5}} >
                                        <Image source={ itemData.item.product.URL != "" ? {uri: itemData.item.product.URL} :  require('../../assets/profile.jpg')} style={styles.productImage} />
                                        <Text style={{alignSelf: 'center', marginVertical: 3}} > {itemData.item.product.title} </Text>
                                    </View>
                                    <View style={styles.cpContainer} >
                                        <Text style={styles.countPrice} > {itemData.item.quantity} </Text>
                                    </View>
                                    <View style={styles.cpContainer} >
                                        <Text style={styles.countPrice} > {itemData.item.product.price} </Text>
                                    </View>
                                </View>
                            ) }
                            ListHeaderComponent={getHeader()}
                        />
                </Modal>
            </View>
            <View>
                <Modal isVisible={showUserInfo} style={styles.modal2} backdropOpacity={0.2} >
                    <View style={styles.crossButton} >
                        <TouchableOpacity onPress={() => setShowUserInfo(false) } >
                            <Entypo name="cross" size={24} color="black" />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.imageContainer} >
                        <Image source={profileImage != "" ? {uri: profileImage} : require('../../assets/profile.jpg')} style={styles.image} />
                    </View>

                    <View style={styles.phoneNumberContainer} >
                        <Text>{phoneNum}</Text>
                    </View>

                    <View style={styles.addressContainer} >
                        <View>
                            <Text> {address.province}/ {address.city} / {address.address} </Text>
                        </View>

                    </View>
                </Modal>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 5,
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    touchable: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between'
    },
    image2: {
        height:50,
        width: 50,
        borderRadius: 25,
        marginTop: 7, 
        marginLeft: 4
    },
    modal1: {
        backgroundColor:'white',
        borderRadius:10, 
        maxHeight: 450, 
        marginTop: 40
    },
    productImage: {
        height:75, 
        width: 75, 
        marginVertical: 3, 
        marginHorizontal: 5
    },
    cpContainer: {
        marginLeft: 47, 
        justifyContent: 'center', 
        paddingBottom: 13
    },
    countPrice: {
        paddingVertical: 2, 
        alignSelf: 'center', 
        marginTop: 10
    },
    header: {
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center', 
        marginLeft: 20
    },
    modal2: {
        backgroundColor:'white',  
        maxHeight: 300, 
        marginTop: 80
        },
    crossButton: {
        height: 20, 
        width:25,
        marginTop: -59,
        marginLeft: -2
    },
    imageContainer: {
        width: 160,
        height: 160, 
        borderRadius: 80,
        overflow:'hidden',
        borderColor:'black',
        borderWidth: 2,
        alignSelf:'center',
        marginVertical: 10
    },
    image: {
        flex: 1, 
        height: undefined,
        width: undefined
    },
    phoneNumberContainer: {
        alignItems: 'center'
    },
    addressContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-evenly', 
        paddingHorizontal: 3 , 
        marginVertical: 4,
        marginHorizontal: 4
    }
})


export default ProductModal;