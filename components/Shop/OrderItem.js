import React, { useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';

import CartItem from './CartItem';
import Colors from '../../constants/Colors';

const OrderItem = ({total, date, items, status}) => {
    const [showDetails, setShowDetails] = useState(false)
    return (
        <View style={styles.orderItem} >
            <View style={styles.summary} >
                <Text style={styles.total}>{total}</Text>
                <Text style={styles.date}>{date}</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems:'center'}} >
                <Button color={Colors.primary} title={ showDetails ? "Hide Details" :  "Show Details"} onPress={()=>setShowDetails(prevState => !prevState) } />
                <FontAwesome name={status ? "check-circle-o" : "motorcycle"} color={status ? "#70E852" : "#FF0000" } size={23} style={{marginLeft: 20}} />
                {/* <View style={{marginRight: 10, justifyContent: 'center'}} >
                </View> */}
            </View>
            {showDetails && 
                <View style={styles.detailItems} >
                    {items.map( cartItem => <CartItem 
                        key={cartItem.id} 
                        image={cartItem.product.image}
                        quantity={cartItem.quantity}
                        title={cartItem.product.title}
                        amount={parseInt(cartItem.product.price) * cartItem.quantity}
                    /> )}
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    orderItem: {
        elevation: 5,
        borderRadius: 10,
        padding: 10,
        margin: 20,
        alignItems: 'center'
    },
    summary: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        marginBottom: 15
    },
    count: {
        fontWeight: 'bold',
        fontSize: 16
    },
    total: {
        fontWeight: 'bold',
        fontSize: 16
    },
    date: {
    fontSize: 16,
    color: '#888' 
    },
    detailItems: {
        width: '100%',
        marginTop: 10
    }
})

export default OrderItem;