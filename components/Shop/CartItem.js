import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const CartItem = props => {
    return(
        <View style={styles.cartItem} >
            <View style={styles.itemData1} >
                <Image style={styles.image} source={{uri:props.image}} />
                <Text style={styles.quantity} > {props.quantity} </Text>
                <Text style={styles.title} > {props.title} </Text>
            </View>
            <View style={styles.itemData2} >
                <Text style={styles.amount} > ${props.amount} </Text>
                {props.action && (
                    <View style={styles.touchable}>
                        <TouchableOpacity onPress={props.onAdd} style={styles.deleteButton} >
                            <Ionicons 
                                name='md-add'
                                size={18}
                                color='black'
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={props.onRemove} style={styles.deleteButton} >
                            <Ionicons 
                                name='md-remove'
                                size={18}
                                color='black'
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={props.onDelete} style={styles.deleteButton} >
                            <Ionicons 
                                name='md-trash'
                                size={23}
                                color='red'
                            />
                        </TouchableOpacity>
                    </View>
                )
                }
            </View>
        </View>
    )

}

const styles = StyleSheet.create({
    cartItem: {
        padding: 7,
        backgroundColor: 'white',
        justifyContent: 'space-between',
        marginHorizontal: 5,
    },
    itemData1: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    itemData2: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    quantity: {
        marginLeft: 4,
        fontWeight: '200',
        color: '#888',
        fontSize: 16
    },
    image: {
        width: 60,
        height: 60,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    amount: {
        fontSize: 16,
        fontWeight: 'bold',
        marginRight: 10
    },
    deleteButton: {
        marginRight: 10
    },
    touchable: {
        flexDirection: 'row'
    }
})

export default CartItem;