import React from 'react';
import 'react-native-gesture-handler'
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Provider as PaperProvider } from 'react-native-paper';
import ReduxThunk from 'redux-thunk';

import categoryReducer from './store/reducers/category';
import authReducer from './store/reducers/auth';
import cartReducer from './store/reducers/cart';
import productsReducer from './store/reducers/products';
import ordersReducer from './store/reducers/orders';
import salesReducer from './store/reducers/sale';

import ShopNavigator from './navigation/ShopNavigator';

const rootReducer = combineReducers({
  products: productsReducer,
  auth: authReducer,
  category: categoryReducer,
  cart: cartReducer,
  orders: ordersReducer,
  sales: salesReducer

});

console.disableYellowBox = true;
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
  return (
    <Provider store={store}>
      <PaperProvider>
        <ShopNavigator />
      </PaperProvider>
    </Provider>
  );
}