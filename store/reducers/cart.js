import { ADD_TO_CART, REMOVE_FROM_CART, ADD_BY_ONE, REMOVE_BY_ONE, RETRIEVE_CART, ORDER, SELECT_ADDRESS } from "../actions/cart";
import {DELETE_PRODUCT} from '../actions/products';
import { ADD_ORDER } from "../actions/orders";

const initialState = {
    items: {},
    total: 0,
    Address: '',
    selectedAddress: {}
}

export default (state = initialState, action) => {
    switch (action.type){
        case RETRIEVE_CART: 
        return {
            ...state,
            items: action.retrievedCart,
            total: action.total
        }
        case ADD_TO_CART:
            return {
                items: {
                    ...state.items,
                    [action.product.id] : action.changedItem
                },
                total: state.total + action.productPrice
                }
            
        case REMOVE_FROM_CART:
            return {
                ...state,
                items: action.removed,
                total: state.total - action.difference
                }
        case ADD_BY_ONE:
            const addedItems = { ...state.items, [action.pid]: action.addedItem }
            console.log(state.total)
            return {
                ...state,
                items: addedItems,
                total: state.total + parseInt(action.price)
                }
        case REMOVE_BY_ONE:
            return {
                ...state,
                items: action.items,
                total: state.total - action.prodPrice
                }
        case ADD_ORDER:
            return initialState
        case DELETE_PRODUCT: {
            return {
                ...state,
                items: action.updatedCartItems,
                total: state.total - action.itemTotal
            }
        }
        case ADD_ORDER: {
            return initialState
        }
        case SELECT_ADDRESS: {
            return {
                ...state,
                selectedAddress: action.address
            }
        }
        default:
            return state
    }
}
