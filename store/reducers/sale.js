import { LOAD_SALES_RECORD, UPDATE_STATUS } from "../actions/sale"

const initialState = {
    soldProduct: []
}

export default (state = initialState, action) => {
    switch(action.type){
        case LOAD_SALES_RECORD:
            return {
                ...state,
                soldProduct: action.soldProduct
            }
        case UPDATE_STATUS:
            return state
        default:
            return initialState;
    }
}