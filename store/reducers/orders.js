import { ADD_ORDER, RETRIEVE_ORDERS } from "../actions/orders"


const initialState = {
    orders: []
}


export default ( state = initialState, action ) => {
    switch (action.type) {
        case ADD_ORDER:
            return {
                orders: action.newOrderList
            }
        case RETRIEVE_ORDERS: {
            return {
                orders: action.orders
            }
        }
        default:
            return state
    }
}