import PRODUCTS from '../../data/dummy-data';
import { 
    LOAD_PRODUCT, 
    SEARCH_PRODUCT, 
    FETCH_PRODUCT_BY_CATEGORY, 
    GET_RATE, ADD_RATE, 
    CHECK_LIKE_DISLIKE, 
    FETCH_PRODUCT_COMMENTS, 
    LIKE, DISLIKE, 
    DELETE_PRODUCT, 
    ADD_COMMENT, 
    LOAD_USER_PRODUCT, 
    EDIT_PRODUCT, 
    UPDATE_PRODUCT,
    CREATE_PRODUCT,
    FETCH_TOTAL_SEARCHED_PRODUCTS,
    FETCH_TOTAL_AVAILABLE_PRODUCTS,
    CHANGE_STATUS,
    CHANGE_SEARCH_STATUS,
    SET_SEARCH_PAGE

 } from '../actions/products';

const initialState = {
    availableProducts: [],
    totalAvailableProducts: 0,
    pageNoAvailableProducts: 1,
    searchedProducts: [],
    totalSearchedProducts: 0,
    pageNoSearchedProducts: 1,
    userProducts: [],
    productComment : [],
    productRate: 0,
    isLiked: false,
    isDisliked: false,
    none: false,
    selectedProduct: {
        title: '',
        price: null,
        count: null,
        description: '',
        image: '',
        tags: []
    }
};

export default (state = initialState, action) => {
    switch(action.type){
        case LOAD_PRODUCT:
            return{
                ...state,
                availableProducts : action.products,
                
            }
        case LOAD_USER_PRODUCT: {
            return {
                ...state,
                userProducts: action.userProducts
            }
        }
        case EDIT_PRODUCT: {
            return {
                ...state,
                selectedProduct: {
                    title: action.title,
                    price: action.price,
                    count: action.count,
                    description: action.description,
                    image: action.image,
                    tags: action.tags
                }
            }
        }
        case UPDATE_PRODUCT: {
            return state
        }
        case CREATE_PRODUCT: {
            return state;
        }
        case FETCH_PRODUCT_BY_CATEGORY:
            return{
                ...state,
                availableProducts: action.productData
            }
        case SEARCH_PRODUCT:
            state.searchedProducts.length = 0
            return {
                ...state,
                searchedProducts: action.productData
            }
        case FETCH_PRODUCT_COMMENTS: {
            return {
                ...state,
                productComment: action.productComment,
                productRate: action.productRate
            }
        }
        case LIKE: {
            return {
                ...state,
                productComment: action.updatedLikeComment,
                isLiked : action.isLiked,
                isDisliked: action.isDisliked,
                none: action.none
            }
        }
        case DISLIKE: {
            return {
                ...state,
                productComment: action.updatedDislikeComment,
                isLiked: action._isLiked,
                isDisliked: action._isDisliked,
                none: action._none
            }
        }
        case GET_RATE: {
            return {
                ...state,
                productRate: action.rate
            }
        }
        case ADD_RATE: {
            return {
                ...state,
                productRate: action.rating
            }
        }
        case CHECK_LIKE_DISLIKE: {
            if (action.like){
                return {
                    ...state,
                    isLiked: true,
                    isDisliked: false,
                    none: false
                }
            }
            else if (action.dislike) {
                return {
                    ...state,
                    isLiked: false,
                    isDisliked: true,
                    none: false
                }
            }
            else {
                return {
                    ...state,
                    isLiked: false,
                    isDisliked: false,
                    none: true
                }
            }
        }
        case DELETE_PRODUCT: {
            return state;
        }
        case ADD_COMMENT: {
            return state
        }
        case FETCH_TOTAL_SEARCHED_PRODUCTS: {
            return {
                ...state,
                totalSearchedProducts: action.totalSearchedProducts
            }
        }
        case FETCH_TOTAL_AVAILABLE_PRODUCTS: {
            return {
                ...state,
                totalAvailableProducts: action.totalAvailableProducts
            }
        }
        case CHANGE_STATUS: {
            return {
                ...state,
                pageNoAvailableProducts: action.pageNo
            }
        }
        case CHANGE_SEARCH_STATUS: {
            return {
                ...state,
                pageNoSearchedProducts: action.searchPageNo
            }
        }
        case SET_SEARCH_PAGE: {
            return {
                ...state,
                pageNoSearchedProducts: action.set
            }
        }
        default:
            return state;

    }
};