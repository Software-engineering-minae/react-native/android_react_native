import { LOAD_CATEGORY } from "../actions/category"

const initialState = {
    clothe:{},
    handicrafts:{},
    foodstuff:{},
    others: {}
}

export default (state=initialState, action) => {
    switch(action.type){
        case LOAD_CATEGORY:{
            return {
                clothe:action.clothe,
                handicrafts:action.handicrafts,
                foodstuff:action.foodstuff,
                others:action.others
            }
        }
        default:
            return state
    }
}