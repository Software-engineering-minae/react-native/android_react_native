import { LOGIN, SIGNUP, LOGOUT, RETRIVE, RETRIEVE_PROFILE, UPDATE_PROFILE, ADD_ADDRESS, DELETE_ADDRESS, ADD_CREDIT } from "../actions/auth";

const initialState = {
    token: null,
    userId: null,
    userInfo: {
        id: null,
        email: null,
        firstName: null,
        lastName: null,
        isProducer: null,
        phoneNumber: null,
        username: null,
        image: null,
        credit: null
    },
    Address: [],
    test: "hossein"
};

export default (state = initialState , action) => {
    switch(action.type){
        case "TEST": {
            return {
                ...state,
                test : action.title
            }
        }
        case LOGIN:
            return {
                ...state,
                token: action.token,
            }
        case SIGNUP:
            return {
                ...state,
                userId: action.userId
            }
        case LOGOUT:
            return initialState
        case RETRIVE: {
            return {
                ...state,
                userInfo: {
                    id: action.id,
                    email: action.email,
                    firstName: action.firstname,
                    lastName: action.lastname,
                    isProducer: action.isProducer,
                    phoneNumber: action.phoneNumber,
                    username: action.username,
                }
            }
        }
        case RETRIEVE_PROFILE: {
            return {
                ...state,
                userInfo : {
                    ...state.userInfo,
                    image: action.image,
                    credit: action.credit
                },
                Address: action.address
            }
        }
        case UPDATE_PROFILE: {
            return {
                ...state,
                userInfo: {
                    ...state.userInfo,
                    image: action.image,
                    email: action.email,
                    firstName: action.first_name,
                    lastName: action.last_name,
                    phoneNumber: action.phonenumber,
                    username: action.username,
                    id: action.id
                }
            }
        }
        case ADD_ADDRESS: {
            return {
                ...state,
                Address: action.newAddress
            }
        }
        case DELETE_ADDRESS: {
            return {
                ...state,
                Address : action.removedAddress
            }
        }
        case ADD_CREDIT: {
            return {
                ...state,
                userInfo: {
                    ...state.userInfo,
                    credit: action.newCredit
                }
            }
        }
        default:
            return state
    }
}