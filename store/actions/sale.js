export const LOAD_SALES_RECORD = 'LOAD_SALES_RECORD';
export const UPDATE_STATUS = 'UPDATE_STATUS';


export const updateStatus = (itemsId, value) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const response = await fetch('https://hampa2.pythonanywhere.com/orders/1/change_order_status/',{
            method:'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                "order_id_list": itemsId,
                "status": value 
            })
        })
        const resData = await response.json();
        console.log("this is resData when Update the status : ", resData)
        let updateStatus = [];
        dispatch({
            type: UPDATE_STATUS,
        })
    }
}

export const loadSalesRecord = () => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const response = await fetch('https://hampa2.pythonanywhere.com/orders/retrieve_sales_record/',{
            method:'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
        const resData = await response.json();
        console.log("this is resData when go to salesRecord : ", resData)
        dispatch({
            type: LOAD_SALES_RECORD,
            soldProduct: resData
        })
    }
}