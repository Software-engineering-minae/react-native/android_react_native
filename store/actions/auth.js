export const SIGNUP = "SIGNUP";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const RETRIVE = "RETRIVE";
export const RETRIEVE_PROFILE = 'RETRIEVE_PROFILE'; 
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const ADD_ADDRESS = 'ADD_ADDRESS';
export const DELETE_ADDRESS = 'DELETE_ADDRESS';
export const ADD_CREDIT = 'ADD_CREDIT'; 

import {Alert} from 'react-native';

export const signup = (firstname, lastname, username, password, phoneNumber, email, is_producer) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/accounts/signup/',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                first_name: firstname,
                last_name: lastname,
                username: username,
                password: password,
                phoneNumber: phoneNumber,
                email: email,
                is_producer: is_producer ? 1 : 0
            })
        })
        const resData = await response.json();
        console.log(resData);
        console.log(response.ok)
        if(!response.ok){
            const errorResData = await response.json();
            const messagew = errorResData.username[0]
            console.log('this is the errorData when want to sign up with an existed username', errorResData)
            let message = 'Something went wrong!'
            if (messagew === 'A user with that username already exists.'){
                message = 'This user has already signed up!'
            }
            throw new Error(message)
        }
        else{
            Alert.alert("Account Successfully created!","Now switch to login", [{
                text: 'Okay'
            }])
        }
        dispatch({
            type:SIGNUP,
            userId: resData.id
        })

    }
}


export const login = (username, password) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/accounts/login/',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        })
        const resData = await response.json();
        if(resData.detail) {
            if(resData.detail == "No active account found with the given credentials"){
                throw new Error("Incorrect username or password!")
            }
            else {
                throw new Error("Login Failed!")

            }
        }else {
            dispatch({
                type: LOGIN,
                token: resData.access
            })
        }
    }
}


export const logout = () => {
    return {
        type: LOGOUT
    }
}


export const retriveInfo = () => {
    return async (dispatch, getState) => {
        const token = getState().auth.token
        const response = await fetch('https://hampa2.pythonanywhere.com/accounts/retrieve/', {
                method: 'GET',
                headers: {
                    'Authorization': "Bearer " + token
                }
            })
        const resData = await response.json();
        console.log("this is retrieve: ", resData)
        dispatch({
            type: RETRIVE,
            id: resData.id,
            email: resData.email,
            firstname: resData.first_name,
            lastname: resData.last_name,
            phoneNumber: resData.phone_number,
            isProducer: resData.is_producer,
            username: resData.username,
        })
    }
}

export const retrieveProfile = () => {
    return async (dispatch, getState) => {
        const token = getState().auth.token
        const response = await fetch('https://hampa2.pythonanywhere.com/profile/retrieve_profile/', {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + token
            }
        })

        const resData = await response.json()

        console.log("this is user profile: ",resData)
        let Address = [];
        for (const key in resData.addresses){
            Address.push(resData.addresses[key])
        }
        dispatch({
            type: RETRIEVE_PROFILE,
            image: resData.image,
            address: Address,
            credit: resData.credit
        })
    }
}


export const editUserInfo = (data, passChange, imageChanged) => {
    return async (dispatch, getState) => {
        
        const token = getState().auth.token
        let bodyData = new FormData();
        if (passChange) {
            if(imageChanged) {
                const uri = data.image;
                let uriParts = uri.split('.');
                let fileType = uriParts[uriParts.length - 1];
                bodyData.append('image', {
                    uri,
                    name: `photo.${fileType}`,
                    type: `image/${fileType}`,
                } )
            }
            bodyData.append('first_name', data.name)
            bodyData.append('last_name', data.lastname)
            bodyData.append('email', data.email)
            bodyData.append('username', data.username)
            bodyData.append('password', data.password)
            bodyData.append('phone_number', data.phonenumber)
        }
        else {
            if(imageChanged) {
                const uri = data.image;
                let uriParts = uri.split('.');
                let fileType = uriParts[uriParts.length - 1];
                bodyData.append('image', {
                    uri,
                    name: `photo.${fileType}`,
                    type: `image/${fileType}`,
                })
            }
            bodyData.append('first_name', data.name)
            bodyData.append('last_name', data.lastname)
            bodyData.append('email', data.email)
            bodyData.append('username', data.username)
            bodyData.append('phone_number', data.phonenumber)
        }
        const response = await fetch('https://hampa2.pythonanywhere.com/profile/1/update_profile/', {
            method: 'PUT',
            headers: {
                'Authorization': "Bearer " + token,
                'Content-Type' : 'multipart/form-data'
            },
            body: bodyData
        })
        const resData = await response.json()
        console.log('this is resData when i click submit edit:', resData)

        dispatch({
            type: UPDATE_PROFILE,
            image: resData.image,
            id: resData.id,
            first_name: resData.user.first_name,
            last_name: resData.user.last_name,
            email: resData.user.email,
            phonenumber: resData.user.phone_number,
            username: resData.user.username
        })
    }
}


export const addAddress = (province, city, address) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const oldAddress = getState().auth.Address;
        const response = await fetch('https://hampa2.pythonanywhere.com/address/1/add_address/',{
            method: 'PUT',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': "Bearer " + token,
            },
            body: JSON.stringify({
                "province": province,
                "city": city,
                "address": address
            })
        })
        const resData = await response.json();
        let newAddress = [];
        for (const key in oldAddress) {
            newAddress.push(oldAddress[key])
        }
        newAddress.push(resData)
        dispatch({
            type: ADD_ADDRESS,
            newAddress: newAddress
        })
    }
}


export const deleteAddress = (addressId) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const oldAddress = getState().auth.Address;
        const response = await fetch('https://hampa2.pythonanywhere.com/address/1/remove_address/',  {
            method: 'DELETE',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': "Bearer " + token,
            },
            body: JSON.stringify({
                "address_id" : addressId
            })
        })
        const resData = await response.json();
        console.log("this is resData when i click delete: ", resData)
        let removedAddress = [];
        if (resData.message === "Address deleted successfully!") {
            for ( const key in oldAddress){
                if (oldAddress[key].id !== addressId ) {
                    removedAddress.push(oldAddress[key])
                }
            }
        }
        else {
            return (
                Alert.alert("Error!","Something went wrong!", [{
                    text: 'Okay'
                }])
            )
        }
        dispatch({
            type: DELETE_ADDRESS,
            removedAddress: removedAddress
        })
    }
}

export const addCredit = (credit) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const response = await fetch('https://hampa2.pythonanywhere.com/profile/1/add_credit/',{
            method: 'PUT',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': "Bearer " + token,
            },
            body: JSON.stringify({
                "additional_credit" : credit
            })
        })
        const resData = await response.json();
        console.log('this is when add credit take place: ', resData)
        dispatch({
            type: ADD_CREDIT,
            newCredit: credit
        })
    }
}