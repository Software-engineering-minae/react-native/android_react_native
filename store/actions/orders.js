import { Alert } from "react-native";

export const ADD_ORDER = 'ADD_ORDER';
export const RETRIEVE_ORDERS = 'RETRIEVE_ORDERS';


export const addOrder = (total, addressId) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token
        console.log("this is selectedAddress id: ", addressId)
        const orderResponse = await fetch('https://hampa2.pythonanywhere.com/orders/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                "total": total,
                "address_id": addressId
            })
        })
        const orderResData = await orderResponse.json();
        console.log("this is orderResData: ", orderResData)
        if (orderResData.Message == "Insufficient Credit!") {
            Alert.alert("Low Credit!","Insufficient Credit!", [{
                text: 'Okay'
            }])
        }
        else {
            Alert.alert("Submitted successfully!","",[{
                text: 'Okay'
            }])
            const response = await fetch ('https://hampa2.pythonanywhere.com/orders/order_history/', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            })
            let newOrderList = [];
            const resData = await response.json();
            for (const key in resData) {
                newOrderList.push(resData[key])
            }
            dispatch({
                type: ADD_ORDER,
                newOrderList: newOrderList
            })
        }


    }
}

export const retrieveOrders = () => {
    return async (dispatch, getState) => {
        const token = getState().auth.token
        const response = await fetch('https://hampa2.pythonanywhere.com/orders/order_history/', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })

        const resData = await response.json();
        let orders = [];
        for (const key in resData){
            orders.push(resData[key])
        }
        console.log("this is order history when i come to home screen: ", resData)
        console.log("this is orders: ", orders)
        dispatch({
            type: RETRIEVE_ORDERS,
            orders: orders
        })
        
    }
}