export const LOAD_CATEGORY = "LOAD_CATEGORY"
export const fetchCategory = () => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/category/');
        const resData = await response.json();
        console.log("this is resData in category: ", resData)
        let clothe;
        let foodstuff;
        let handicrafts;
        let others;
        for (const key in resData) {
            if(resData[key].tag == 'foodstuff') {
                foodstuff = {
                    type: 'foodstuff',
                    image: resData[key].URL
                }
            }
            if(resData[key].tag == 'handcrafts') {
                handicrafts = {
                    type: 'handcrafts',
                    image: resData[key].URL
                }
            }
            if(resData[key].tag == 'clothes') {
                clothe = {
                    type: 'clothes',
                    image: resData[key].URL
                }
            }
            if(resData[key].tag == 'others') {
                others = {
                    type: 'others',
                    image: resData[key].URL
                }
            }
        }
        dispatch({
            type:LOAD_CATEGORY,
            clothe: clothe,
            handicrafts: handicrafts,
            foodstuff: foodstuff,
            others: others
        })
    }
}

