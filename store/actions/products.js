import Product from "../../models/product";
import UserProduct from "../../models/userProduct";
import Comment from "../../models/comment";
import { Alert } from "react-native";
 

export const LOAD_PRODUCT = 'LOAD_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const FETCH_PRODUCT_BY_CATEGORY = 'FETCH_PRODUCT_BY_CATEGORY';
export const SEARCH_PRODUCT = 'SEARCH_PRODUCT'; 
export const FETCH_PRODUCT_COMMENTS = 'FETCH_PRODUCT_COMMENTS'; 
export const LIKE = 'LIKE';
export const DISLIKE = 'DISLIKE';
export const GET_RATE = 'GET_RATE';
export const ADD_RATE = 'ADD_RATE';
export const CHECK_LIKE_DISLIKE = 'CHECK_LIKE_DISLIKE';
export const ADD_COMMENT = 'ADD_COMMENT'; 
export const LOAD_USER_PRODUCT = 'LOAD_USER_PRODUCT';
export const EDIT_PRODUCT = 'EDIT_PRODUCT';
export const FETCH_TOTAL_SEARCHED_PRODUCTS = 'FETCH_TOTAL_SEARCHED_PRODUCTS';
export const FETCH_TOTAL_AVAILABLE_PRODUCTS = 'FETCH_TOTAL_AVAILABLE_PRODUCTS';
export const CHANGE_STATUS = 'CHANGE_STATUS';
export const CHANGE_SEARCH_STATUS = 'CHANGE_SEARCH_STATUS';
export const SET_SEARCH_PAGE = 'SET_SEARCH_PAGE';



export const loadProduct = ()=> {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/')
        const resData = await response.json();
        const loadedProducts = []
        let i = 0;
        for(const prod in resData) {
            loadedProducts.push(new Product(resData[prod].id, resData[prod].owner, resData[prod].title, resData[prod].image, resData[prod].detail, resData[prod].price, resData[prod].tags[0]))
        }
        dispatch({
            type: LOAD_PRODUCT,
            products: loadedProducts
        })
    }
}

export const fetchTotalAvailableProducts = (category) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/total/', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                cat: category
            })
        })
        const resData = await response.json();
        console.log("this is total Available Products: ",resData)
        dispatch({
            type: FETCH_TOTAL_AVAILABLE_PRODUCTS,
            totalAvailableProducts: resData.product_number
        })
    }
}

export const setSearchPage = () => {
    return {
        type: SET_SEARCH_PAGE,
        set: 1
    }
}

export const changePage = (pageNo,status) => {
    if(status == '+'){
        return {
            type: CHANGE_STATUS,
            pageNo: pageNo
        }
    }
    if(status == '-'){
        return {
            type: CHANGE_STATUS,
            pageNo: pageNo
        }
    }
}

export const changeSearchPage = (pageNo,status) => {
    if(status == '+'){
        return {
            type: CHANGE_SEARCH_STATUS,
            searchPageNo: pageNo
        }
    }
    if(status == '-'){
        return {
            type: CHANGE_SEARCH_STATUS,
            searchPageNo: pageNo
        }
    }
}

export const fetchProductByCategoryType = (catType,pageNo) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/category/', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                choice: catType,
                page: pageNo
            })
            
        })
        const resData = await response.json()
        console.log(resData)
        const loadedProducts = []
        for(const key in resData){
            loadedProducts.push(new Product(resData[key].id, resData[key].owner, resData[key].title, resData[key].URL, resData[key].detail, resData[key].price, resData[key].tags[0], resData[key].hashtags))
        }
        dispatch({
            type:FETCH_PRODUCT_BY_CATEGORY,
            productData: loadedProducts
        })
    }
}

export const fetchTotalSearchedProducts = (text) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/total-search/', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                input : text
            })
        })
        const resData = await response.json();
        console.log("this is total searched products: ", resData)
        dispatch({
            type: FETCH_TOTAL_SEARCHED_PRODUCTS,
            totalSearchedProducts: resData.product_number
        })
    }
}

export const searchProduct = (text, pageNo) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/search/',{
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                input: text,
                page: pageNo
            })
        })
        const resData = await response.json()
        console.log("this is resData when i press search: ", resData)
        const loadedProducts = []
        for (const key in resData) {
            loadedProducts.push(new Product(resData[key].id, resData[key].owner, resData[key].title, resData[key].URL, resData[key].detail, resData[key].price, resData[key].tags[0], resData[key].hashtags))
        }
        dispatch({
            type: SEARCH_PRODUCT,
            productData: loadedProducts,
        })
    }
}
export const fetchComment = (prodId) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/comments/', {
            method:'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                productID: prodId
            })
        })
        const resData = await response.json();
        const prodComment = [];
        for (const key in resData){
            prodComment.push(new Comment(resData[key].id, resData[key].like, resData[key].dislike, resData[key].user.username, resData[key].text ))
        }

        dispatch({
            type: FETCH_PRODUCT_COMMENTS,
            productComment: prodComment,
        })
    }
}

export const checkLikeOrDislike = (comment) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const response = await fetch('https://hampa2.pythonanywhere.com/products/check-likes/',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                commentID: comment.id
            })
        })
        let like = false;
        let dislike = false;
        let none = false;
        const resData = await response.json();
        if(resData.dislike){
            dislike = true;
            dispatch({
                type: CHECK_LIKE_DISLIKE,
                dislike : dislike
            }) 
        }
        else if (resData.like) {
            like = true;
            dispatch({
                type: CHECK_LIKE_DISLIKE,
                like: like
            })
        }
        else {
            none = true;
            dispatch({
                type: CHECK_LIKE_DISLIKE,
                none : none
            })
        }
    }
}

export const like = (comment, allComments) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const isLiked = getState().products.isLiked;
        const isDisliked = getState().products.isDisliked;
        const none = getState().products.none

        let isLiked_Copy = isLiked;
        let isDisliked_Copy = isDisliked;
        let none_Copy = none;

        const response = await fetch('https://hampa2.pythonanywhere.com/products/add-likes/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                commentID: comment.id,
                like: "like"
            })
        })
        const updateLikeCount = [];

        if(isLiked){
            for (const key in allComments){
                updateLikeCount.push(new Comment(allComments[key].id, allComments[key].like, allComments[key].dislike, allComments[key].commenter, allComments[key].comment ))
            }
            Alert.alert("NO!", "You can't like or dislike more than once!", [{
                text: 'Okay'
            }])
        }
        else if (isDisliked) {
            for (const key in allComments){
                if (comment.id == allComments[key].id){
                    updateLikeCount.push(new Comment(comment.id, comment.like + 1, comment.dislike - 1, comment.commenter, comment.comment))
                }
                else {
                    updateLikeCount.push(new Comment(allComments[key].id, allComments[key].like, allComments[key].dislike, allComments[key].commenter, allComments[key].comment ))
                }
            }
            isDisliked_Copy = false;
            isLiked_Copy = true;
        }
        else if (none) {
            for (const key in allComments) {
                if (comment.id == allComments[key].id){
                    updateLikeCount.push(new Comment(comment.id, comment.like + 1, comment.dislike , comment.commenter, comment.comment))
                }
                else {
                    updateLikeCount.push(new Comment(allComments[key].id, allComments[key].like, allComments[key].dislike, allComments[key].commenter, allComments[key].comment))
                }
            }
            isLiked_Copy = true;
            none_Copy = false;
        }

        dispatch({
            type: LIKE,
            updatedLikeComment: updateLikeCount,
            isLiked: isLiked_Copy,
            isDisliked: isDisliked_Copy,
            none: none_Copy
        })
    }
        
} 


export const dislike = (comment, allComments) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const isLiked = getState().products.isLiked;
        const isDisliked = getState().products.isDisliked;
        const none = getState().products.none

        let _isLikedCopy = isLiked;
        let _isDislikedCopy = isDisliked;
        let _noneCopy = none;

        const response = await fetch('https://hampa2.pythonanywhere.com/products/add-likes/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                commentID: comment.id,
                like: "dislike"
            })
        })

        const updatedDislikeCount = []
        if (isLiked){
            for (const key in allComments) {
                if(comment.id == allComments[key].id) {
                    updatedDislikeCount.push(new Comment(comment.id, comment.like - 1, comment.dislike + 1, comment.commenter, comment.comment))
                }
                else {
                    updatedDislikeCount.push(new Comment(allComments[key].id, allComments[key].like, allComments[key].dislike, allComments[key].commenter, allComments[key].comment))
                }
            }
            _isDislikedCopy = true;
            _isLikedCopy = false;
        }
        else if (isDisliked) {
            for (const key in allComments){
                updatedDislikeCount.push(new Comment(allComments[key].id, allComments[key].like, allComments[key].dislike, allComments[key].commenter, allComments[key].comment))
            }
            Alert.alert("No!","You can't like or dislike more than once!", [{
                text: 'Okay'
            }])
        }
        else if (none) {
            for (const key in allComments) {
                if (comment.id == allComments[key].id) {
                    updatedDislikeCount.push(new Comment(comment.id, comment.like, comment.dislike + 1, comment.commenter, comment.comment))
                }
                else {
                    updatedDislikeCount.push(new Comment(allComments[key].id, allComments[key].like, allComments[key].dislike, allComments[key].commenter, allComments[key].comment))
                }
            }
            _noneCopy = false;
            _isDislikedCopy = true;
        }

        dispatch({
            type: DISLIKE,
            updatedDislikeComment: updatedDislikeCount,
            _isLiked : _isLikedCopy,
            _isDisliked : _isDislikedCopy,
            _none : _noneCopy 
        })
    }
}



export const getProductRate = (prodId) => {
    return async dispatch => {
        const response = await fetch('https://hampa2.pythonanywhere.com/products/rate/',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                productID: prodId
            })
        })

        const resData = await response.json();
        dispatch({
            type: GET_RATE,
            rate: resData.rate
        })
    }
}

export const addRate = (prodId, rate) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const response = await fetch('https://hampa2.pythonanywhere.com/products/add-rate/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                productID: prodId,
                rate: rate
            })
        })

        const resData = await response.json();
        if(resData.error){
            Alert.alert("No!","You can't rate this app more than once!", [{
                text: 'Okay'
            }])
        }
        else{
        dispatch({
            type: ADD_RATE,
            rating: rate
        }) 
    }
}
}

export const addComment = (prodId, text) => {
    return async (dispatch,getState) => {
        const token = getState().auth.token
        const response = await fetch('https://hampa2.pythonanywhere.com/products/add-comment/', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                productID: prodId,
                text: text
            })
        })
        dispatch({
            type: ADD_COMMENT
        })
    }
}


export const loadUserProduct = () => {
    return async (dispatch, getState) => {
        const token = getState().auth.token
        const response = await fetch('https://hampa2.pythonanywhere.com/producers/user-products/',{
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
        const resData = await response.json()
        console.log("userProducts: ", resData)
        let userProducts = [];
        for(const prod in resData) {
            userProducts.push(new UserProduct(resData[prod].id, resData[prod].owner, resData[prod].title, resData[prod].URL, resData[prod].detail, resData[prod].price, resData[prod].tags[0], resData[prod].count))
        }
        dispatch({
            type: LOAD_USER_PRODUCT,
            userProducts: userProducts
        })
    }
}

export const createProduct = (uri, title, price, quantity, description, tag) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        let tags = [];
        if(tag == 'foodstuff'){
            tags.push(1)
        }
        else if ( tag == 'clothes' ) {
            tags.push(2)
        }
        else if ( tag == 'handcrafts' ) {
            tags.push(3)
        }
        else {
            tags.push(4)
        }

        let uriParts = uri.split('.');
        let fileType = uriParts[uriParts.length - 1];

        let formData = new FormData();
        formData.append('image', {
            uri,
            name: `photo.${fileType}`,
            type: `image/${fileType}`,
        });
        formData.append('title',title)
        formData.append('detail', description)
        formData.append('price', price)
        formData.append('count', quantity)
        formData.append('tags', tags[0])
        console.log("this is form data when i click create!: ", formData)
        const response = await fetch('https://hampa2.pythonanywhere.com/producers/add-product/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            },
            body: formData
            })
        const resData = await response.formData();
        console.log("this is when i click create! : ", resData)
        dispatch({
            type: CREATE_PRODUCT
        })
        
    }
}


export const editProduct = (productId) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token
        const response = await fetch('https://hampa2.pythonanywhere.com/producers/user-products/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                productID: productId
            })
        })

        const resData = await response.json();
        console.log('this is resData when i press edit: ', resData)
        dispatch({
            type: EDIT_PRODUCT,
            title: resData.title,
            price: resData.price,
            count: resData.count.toString(),
            description: resData.detail,
            image: resData.URL,
            tags: resData.tags
        })
    }
}


export const updateProduct = (uri, title, price, quantity, description, tag, id) => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        console.log("this is tag: ",tag)
        let tags = [];
        if(tag == 'foodstuff'){
            tags.push(1)
        }
        else if ( tag == 'clothes' ) {
            tags.push(2)
        }
        else if ( tag == 'handcrafts' ) {
            tags.push(3)
        }
        else {
            tags.push(4)
        }

        let uriParts = uri.split('.');
        let fileType = uriParts[uriParts.length - 1];

        let formData = new FormData();
        if (!uri == '') {
            formData.append('image', {
                uri,
                name: `photo.${fileType}`,
                type: `image/${fileType}`,
            });
        }
        formData.append('title',title)
        formData.append('detail', description)
        formData.append('price', price)
        formData.append('count', parseInt(quantity))
        formData.append('tags', tags[0])
        formData.append('productID', id)

        console.log("this is form data when i click submit: ", formData)
        const response = await fetch('https://hampa2.pythonanywhere.com/producers/update-product/', {
            method: 'PUT',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            },
            body: formData
            })
        if (!response.ok){
            console.log(response)
        }
        
        
        // console.log("this is response when i click submit: ", response)
        const resData = await response.formData();
        console.log("this is resData when i click submmit: ",resData)
        dispatch({
            type: UPDATE_PRODUCT
        })
        
    }
}


export const deleteProduct = productId => {
    return async (dispatch, getState) => {
        const token = getState().auth.token
        const userProducts = getState().products.userProducts
        const cartItems = getState().cart.items
        let itemTotal = 0;
        let updatedCartItems = cartItems
        if(cartItems[productId]){
            itemTotal = cartItems[productId].sum
            delete updatedCartItem[productId]
        }
        await fetch('https://hampa2.pythonanywhere.com/producers/delete-product/',{
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({
                productID: productId
            })
        })
        dispatch({
            type: DELETE_PRODUCT,
            itemTotal: itemTotal,
            updatedCartItems: updatedCartItems
        })
    }
}