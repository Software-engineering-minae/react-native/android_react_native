import { Alert } from "react-native";
import CartItem from "../../models/cart";

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ADD_BY_ONE = 'ADD_BY_ONE';
export const REMOVE_BY_ONE = 'REMOVE_BY_ONE';
export const RETRIEVE_CART = 'RETRIEVE_CART';
export const ORDER = 'ORDER';
export const SELECT_ADDRESS = 'SELECT_ADDRESS';


export const retieveCart = () => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const response = await fetch('https://hampa2.pythonanywhere.com/carts/retrieve_cart/',{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
        let retrieve = {};
        let total = 0;
        const resData = await response.json()
        console.log("this is resData when i go to cart screen: ", resData)
        for (const key in resData.cart_items){
            retrieve = {...retrieve, [resData.cart_items[key].product.id]: new CartItem(
                resData.cart_items[key].quantity,
                resData.cart_items[key].product.URL,
                resData.cart_items[key].product.price,
                resData.cart_items[key].product.title,
                resData.cart_items[key].product.detail,
                resData.cart_items[key].product.price * resData.cart_items[key].quantity
            )}
            total += parseInt(resData.cart_items[key].product.price) * resData.cart_items[key].quantity
        }
        dispatch({
            type: RETRIEVE_CART,
            retrievedCart: retrieve,
            total: total
        })
    }
}

export const addToCart = product => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const cartItems = getState().cart.items;
        const prodPrice = parseInt(product.price);
        const prodTitle = product.title;
        const prodDescription = product.description;
        const prodImage = product.imageUrl;

        const getProductInfo = await fetch('https://hampa2.pythonanywhere.com/products/product-detail/',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "productID": product.id
            })
        })
        
        const productInfoData = await getProductInfo.json();
        
        const prodCount = productInfoData.count;

        let updatedOrNewCartItem;

        if ( cartItems[product.id] ){
            if(cartItems[product.id].quantity <= prodCount - 1){
                updatedOrNewCartItem = new CartItem(
                    cartItems[product.id].quantity + 1,
                    prodImage,
                    prodPrice,
                    prodTitle,
                    prodDescription,
                    cartItems[product.id].sum + prodPrice
                )
               await fetch('https://hampa2.pythonanywhere.com/carts/1/add_to_cart/', {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                    body: JSON.stringify({
                        "product_id": product.id,
                        "count": updatedOrNewCartItem.quantity
                    })
                })
                dispatch(
                    {
                        type: ADD_TO_CART,
                        product: product,
                        productPrice: prodPrice,
                        changedItem: updatedOrNewCartItem
                    }
                )
            }
            else {
                Alert.alert("No!",`You can add ${prodCount} at most!`, [{
                    text: 'Okay'
                }])
            }
        }
        else {
            if(prodCount != 0){
                updatedOrNewCartItem = new CartItem(1, prodImage, prodPrice, prodTitle, prodDescription, prodPrice)
                await fetch('https://hampa2.pythonanywhere.com/carts/1/add_to_cart/', {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                    body: JSON.stringify({
                        "product_id": product.id,
                        "count": updatedOrNewCartItem.quantity
                    })
                }) 
                dispatch(
                    {
                        type: ADD_TO_CART,
                        product: product,
                        productPrice: prodPrice,
                        changedItem: updatedOrNewCartItem
                    }
                )
            }
            else{
                Alert.alert("Can't add item to the cart!", "This perticular Product is not available right now!",[{
                    text:'Okay'
                }])
            }
        }
        
    }
}

export const removeFromCart = productId => {
    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const cartItems = getState().cart.items;
        const selectedItem = cartItems[productId];
        const currentQuantity = selectedItem.quantity;
        let removed;
        removed = cartItems
        delete removed[productId]
        await fetch('https://hampa2.pythonanywhere.com/carts/1/remove_from_cart/',{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    "product_id":productId,
                    "count": 0
                })
            })
        dispatch({
            type: REMOVE_FROM_CART,
            difference: parseInt(selectedItem.productPrice) * currentQuantity,
            removed: removed
        })
    }
}

export const addByOne = productId => {
    return async (dispatch, getState) =>  {
        const token = getState().auth.token;
        const cartItems = getState().cart.items;
        const selectedItem = cartItems[productId];
        const currentQuantity = selectedItem.quantity;
        const getProductInfo = await fetch('https://hampa2.pythonanywhere.com/products/product-detail/',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "productID": productId
            })
        })
        const productInfoData = await getProductInfo.json();
        const prodCount = productInfoData.count;
        console.log(selectedItem)

        let addedCartItem;
        if(currentQuantity <= prodCount - 1){
            addedCartItem = new CartItem(
                currentQuantity + 1,
                selectedItem.productImage,
                selectedItem.productPrice,
                selectedItem.productTitle,
                selectedItem.productDescription,
                parseInt(selectedItem.sum) + parseInt(selectedItem.productPrice)
            )
           await fetch('https://hampa2.pythonanywhere.com/carts/1/add_to_cart/', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    "product_id": productId,
                    "count": addedCartItem.quantity
                })
            })

            dispatch({
                type: ADD_BY_ONE,
                pid: productId,
                addedItem: addedCartItem,
                price: selectedItem.productPrice
            })
    }
    else{
        Alert.alert("No!", `You can add ${prodCount} at most!`, [{
            text: 'Okay'
        }])
    }
}
}

export const removeByOne = productId => {

    return async (dispatch, getState) => {
        const token = getState().auth.token;
        const cartItems = getState().cart.items;
        const selectedItem = cartItems[productId];
        const currentQuantity = selectedItem.quantity;
        let items;
        if(currentQuantity == 1){
            items = cartItems
            delete items[productId]
            await fetch('https://hampa2.pythonanywhere.com/carts/1/reduce_from_cart/',{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    "product_id":productId,
                    "count": 0
                })
            })
        }
        else {
            const removedCartItem = new CartItem(currentQuantity - 1, selectedItem.productImage, selectedItem.productPrice, selectedItem.productTitle, selectedItem.productDescription, selectedItem.sum - selectedItem.productPrice) 
            items = {...cartItems, [productId] : removedCartItem}
            await fetch('https://hampa2.pythonanywhere.com/carts/1/reduce_from_cart/',{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    "product_id":productId,
                    "count": 0
                })
            })
        }
        dispatch({
            type: REMOVE_BY_ONE,
            pid: productId,
            prodPrice: selectedItem.productPrice,
            items: items
        })
    }
}

export const selectAddress = (address) => {
    return {
        type: SELECT_ADDRESS,
        address: address

    }
}

// export const order = (total) => {
//     return async (dispatch, getState) => {
//         const token = getState().auth.token;
//         await fetch('https://hampa2.pythonanywhere.com/orders/',{
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': 'Bearer ' + token
//             },
//             body: JSON.stringify({
//                 "total": total
//             })
//         })
//         Alert.alert("Your order has Successfully registered!","You can check it in your profile", [{
//             text: 'Okay'
//         }])
//         dispatch({
//             type: ORDER,
//             refreshItems: {},
//             refreshTotal: 0
//         })
//     }
// }